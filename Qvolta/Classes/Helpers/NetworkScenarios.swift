//
//  NetworkScenarios.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 3/5/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import Foundation
import ReactiveCocoa
import ReactiveSwift
import Result

struct NetworkScenarios {
    static func logUserIn(_ user: User) -> SignalProducer<User, APIError> {
        let network: NetworkService = ServiceLocator.shared.get()!
        
        return network
            .getTokenBy(login: user.email!, password: user.password!)
            .flatMap(.latest, { (token) -> SignalProducer<User, APIError> in
                network.set(token: token)
                Session.shared.token = token
                
                return network.getUser()
            })
    }
    
    static func showAuthorizedScreen(animated: Bool = true) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let navigationController: UINavigationController = (appDelegate.window?.rootViewController as! UINavigationController)
        
        let controller: AuthorizedTabBarViewController = UIStoryboard(name: "EntryPoint", bundle: nil).instantiateViewController(withIdentifier: "AuthorizedTabBarViewController") as! AuthorizedTabBarViewController
        
        navigationController.setViewControllers([controller], animated: animated)
    }
}
