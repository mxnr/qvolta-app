//
//  Token.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 1/25/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import Foundation
import ObjectMapper

struct Token: Mappable {
    let access_token: String
    let refresh_token: String
    let expires_in: Int
    let token_type: String
    let scope: String?
    
    init?(map: Map) {
        self.access_token = try! map.value("access_token")
        self.refresh_token = try! map.value("refresh_token")
        self.expires_in = try! map.value("expires_in")
        self.token_type = try! map.value("token_type")
        self.scope = try? map.value("scope")
    }
    
    mutating func mapping(map: Map) {
        access_token >>> map["access_token"]
        refresh_token >>> map["refresh_token"]
        expires_in >>> map["expires_in"]
        token_type >>> map["token_type"]
        scope >>> map["scope"]
    }
}
