//
//  TradeOrder.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/24/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import UIKit

struct TradeOrder {
    enum OrderType: String {
        case Buy
        case Sell
    }
    
    var user: User!
    var paymentSystem: PaymentSystem!
    var priceCoin: Double!
    var priceCurrency: Double!
    
    var orderType: OrderType!
    
    var coin: Coin!
}
