//
//  PaymentSystem.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/24/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import UIKit
import ObjectMapper

struct PaymentSystem: Mappable {
    init?(map: Map) {
        self.id = try! map.value("id")
        self.name = try! map.value("name")
        self.section = try? map.value("section")
    }
    
    mutating func mapping(map: Map) {
        
    }
    
    var id: UInt16
    var name: String
    var section: String?
    var logo: UIImage?
    
    
}

