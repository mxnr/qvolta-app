//
//  CoinSettings.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/2/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import UIKit

enum Coin: String {
    case BTC
    case ETH
}

extension Coin {
    var settings: CoinThemeSettings {
        switch self {
        case .BTC:
            return Bitcoin()
        case .ETH:
            return Etherium()
        }
    }
}

class WalletCoin {
//    var themeSettings: CoinThemeSettings!
}

protocol CoinThemeSettings {
    var logoBig: UIImage { get }
    var logoSmall: UIImage { get }
    var closeIcon: UIImage { get }
    var code: String { get }
    var name: String { get }
    var firstColor: UIColor { get }
    var secondColor: UIColor { get }
    var shadowColor: UIColor { get }
}

struct Bitcoin: CoinThemeSettings {
    var logoBig: UIImage { return #imageLiteral(resourceName: "imgBtcBig") }
    var logoSmall: UIImage { return #imageLiteral(resourceName: "imgBtcSmall") }
    var closeIcon: UIImage { return #imageLiteral(resourceName: "icCloseBitcoin") }
    
    var code: String { return "BTC" }
    var name: String { return "Bitcoin" }
    
    var firstColor: UIColor { return .orange }
    var secondColor: UIColor { return .orangeYellow }
    var shadowColor: UIColor { return .pumpkinOrange60 }
}

struct Etherium: CoinThemeSettings {
    var logoBig: UIImage { return #imageLiteral(resourceName: "imgEthBig") }
    var logoSmall: UIImage { return #imageLiteral(resourceName: "imgEthSmall") }
    var closeIcon: UIImage { return #imageLiteral(resourceName: "icCloseEtherium") }
    
    var code: String { return "ETH" }
    var name: String { return "Etherium" }
    
    var firstColor: UIColor { return .indigo }
    var secondColor: UIColor { return .purpleishBlue }
    var shadowColor: UIColor { return .burple60 }
}
