//
//  Currency.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 3/11/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import Foundation
import ObjectMapper

struct Currency: Mappable {
    
    let id: Int16
    let name: String
    
    init?(map: Map) {
        self.id = try! map.value("id")
        self.name = try! map.value("name")
    }
    
    mutating func mapping(map: Map) {
//        id >>> map["id"]
//        name >>> map["name"]
    }
    
}
