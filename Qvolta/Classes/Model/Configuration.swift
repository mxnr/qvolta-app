//
//  Configuration.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 3/11/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import Foundation
import ObjectMapper

struct Configuration: Mappable {
    
    let currencies: [Currency]
    let paymentMethods: [PaymentSystem]
    let countries: [Country]
    
    init?(map: Map) {
        self.currencies = try! map.value("currencies")
        self.countries = try! map.value("countries")
        self.paymentMethods = try! map.value("payment_methods")
    }
    
    mutating func mapping(map: Map) {
        currencies >>> map["currencies"]
        countries >>> map["countries"]
        paymentMethods >>> map["payment_methods"]
    }
    
}
