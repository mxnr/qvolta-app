//
//  TradeSearch.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/26/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import Foundation

class BuySellSearch {
    enum Activity: String {
        case Sell
        case Buy
    }
    
    enum Property: Int {
        case Activity
        case Amount
        case Coin
        case Country
        case Payment
    }
    
    var activity: Activity!
    var amount: Double!
    var coin: Coin!
    var country: Country!
    var payment: PaymentSystem!
    
    init() {
        
    }
}
