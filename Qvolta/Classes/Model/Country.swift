//
//  Country.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 3/11/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import Foundation
import ObjectMapper

struct Country: Mappable {
    
    var id: UInt16
    var name: String
    var code: String
    
    init?(map: Map) {
        self.id = try! map.value("id")
        self.name = try! map.value("name")
        self.code = try! map.value("code")
    }
    
    mutating func mapping(map: Map) {
//        id >>> map["id"]
//        name >>> map["name"]
//        code >>> map["code"]
    }
}
