//
//  LoginRequest.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 1/31/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import Foundation
import ObjectMapper

struct LoginRequest: Mappable {
    
    init(password: String) {
        self.password = password
    }
    
    init?(map: Map) {
        self.password = try? map.value("password")
    }
    
    mutating func mapping(map: Map) {
        password >>> map["password"]
        clientId >>> map["client_id"]
    }
    
    let password: String?
    let clientId: String = Keys.ClientId
    
    
}
