//
//  APIError.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 1/25/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import Foundation
import ObjectMapper
import Result

public protocol APIErrorProtocol: Mappable {
    var code: Int? { get set }
    
    var message: String? { get }
    var debugMessage: String? { get }
    
    init(_ error: Swift.Error)
}

struct APIError: Error, APIErrorProtocol {
    
    init?(map: Map) {
        code = try? map.value("code")
        message = try? map.value("message")
        
        debugMessage = try? map.value("debugMessage")
    }
    
    mutating func mapping(map: Map) { }
    
    public var error: Error?
    
    public var code: Int?
    
    public var message: String?
    public var debugMessage: String?
    
    public init(_ error: Error) {
        if let anyError = error as? APIError {
            self = anyError
        } else {
            self.error = error
            self.message = self.error?.localizedDescription
        }
    }
}

extension APIError: ErrorConvertible {
    public static func error(from error: Error) -> APIError {
        return APIError(error)
    }
}
