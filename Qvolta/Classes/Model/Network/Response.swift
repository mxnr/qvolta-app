//
//  Response.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 1/25/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import Foundation
import ObjectMapper

struct Response: Mappable {
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
    }
}

class ResponseArray<T: Mappable>: Mappable {
    var limit: Int?
    var offset: Int?
    var totalCount: Int?
    
//    var data: [T]?
    var data: T?
    
    required init?(map: Map) {}
    
    init() {}
    
    func mapping(map: Map) {
        limit <- map["limit"]
        offset <- map["offset"]
        totalCount <- map["totalCount"]
        data <- map["data"]
    }
}
