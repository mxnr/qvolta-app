//
//  User.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/2/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import Foundation
import ObjectMapper

struct User: Mappable {
    let id: Int16?
    let username: String
    var email: String?
    var password: String?
    var photoImage: UIImage?
    var isVerified: Bool = false
    
    init?(map: Map) {
        self.id = 16//try! map.value("id")
        self.username = try! map.value("username")
        self.email = try! map.value("email")
//        self.password = try! map.value("password")
    }
    
    init(id: Int16, username: String) {
        self.id = id
        self.username = username
    }
    
    init(email: String, password: String) {
        self.id = 0
        self.username = ""
        self.email = email
        self.password = password
    }
    
    mutating func mapping(map: Map) {
//        id >>> map["id"]
        username >>> map["username"]
        email <- map["email"]
        password <- map["password"]
    }
    
}

struct UserCreateRequest: Mappable {
    let username: String
    let email: String
    let password: String
    
    init?(map: Map) {
        return nil
    }
    
    init(username: String, email: String, password: String) {
        self.username = username
        self.email = email
        self.password = password
    }
    
    mutating func mapping(map: Map) {
        username >>> map["username"]
        email >>> map["email"]
        password >>> map["password"]
    }
}
