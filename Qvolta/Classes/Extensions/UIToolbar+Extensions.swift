//
//  UIToolbar+Extensions.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/22/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import UIKit

extension UIToolbar {
    static func withButton(tintColor: UIColor,
                               target: Any,
                               selector: Selector,
                               title: String = "Next") -> UIToolbar {
        let toolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44))
        toolbar.barStyle = .default
        let nextButton = UIBarButtonItem(title: title,
                                         style: .plain,
                                         target: target,
                                         action: selector)
        nextButton.tintColor = tintColor
        
        toolbar.setItems([UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                          target: nil,
                                          action: nil), nextButton],
                         animated: false)
        
        return toolbar
    }
}
