//
//  UIView+Extensions.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/2/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import UIKit

extension UIView {
    private class func instantiateType<T>() -> T {
        return UINib(nibName: String(describing: self), bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! T
    }
    
    class func viewWithNib() -> Self {
        return self.instantiateType()
    }
}
