//
//  ServiceLocator.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 1/10/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import Foundation

protocol ServiceLocatorProtocol {
    func get<Service>() -> Service?
    func set<Service>(_ service: Service)
}

class ServiceLocator: ServiceLocatorProtocol {
    
    // MARK: - Singleton
    static let shared = ServiceLocator()
    private init() {}
    
    // Service registry
    private lazy var services: [String: Any] = [:]
    
    // MARK: - Public Methods
    
    func set<Service>(_ service: Service) {
        let className: String = String(describing: type(of: service))
        
        services[className] = service
    }
    
    func get<Service>() -> Service? {
        let className = "\(Service.self)"
        
        if let service = services[className] as? Service {
            return service
        }
        
        return nil
    }
    
}
