//
//  MapperService.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 1/25/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

extension DataRequest {

    /// Utility function for extracting JSON from response
    internal static func processResponse(request: URLRequest?, response: HTTPURLResponse?, data: Data?, keyPath: String?) -> Any? {
        let jsonResponseSerializer = DataRequest.jsonResponseSerializer(options: .allowFragments)
        let result = jsonResponseSerializer.serializeResponse(request, response, data, nil)
        
        let JSON: Any?
        if let keyPath = keyPath , keyPath.isEmpty == false {
            JSON = (result.value as AnyObject?)?.value(forKeyPath: keyPath)
        } else {
            JSON = result.value
        }
        
        return JSON
    }
    
    /// BaseMappable Object Serializer
    static func modelSerializer<T: BaseMappable, E: APIErrorProtocol>(type: E.Type?) -> DataResponseSerializer<T> {
        
        return DataResponseSerializer<T> { request, response, data, error in
            
            let JSONObject = processResponse(request: request, response: response, data: data, keyPath: nil)
            if response != nil {
                
                // Check for error parsed response
                switch response!.statusCode {
                case 200..<400:
                    break
                default:
                    if var errorParsed = Mapper<E>().map(JSONObject: JSONObject) {
                        if errorParsed.code == nil {
                            errorParsed.code = response!.statusCode
                        }
                        
                        return .failure(errorParsed as! Error)
                    }
                }
            }
            
            if error != nil {
                let apiError = E(error!)
                return .failure(apiError as! Error)
            }
            
            if let returnObject = Mapper<T>(context: nil, shouldIncludeNilValues: false).map(JSONObject: JSONObject) {
                
                return .success(returnObject)
            }
            
            if response!.statusCode >= 200, response!.statusCode < 300 {
                let statusOk = Mapper<T>(context: nil, shouldIncludeNilValues: false).map(JSONObject: ["status" : "OK"])
                
                return .success(statusOk!)
            }
            
            return .failure(NSError(domain: "", code: response!.statusCode, userInfo: nil))
        }
    }
    
    @discardableResult
    func responseModel<T: BaseMappable, E: APIErrorProtocol>(
        queue: DispatchQueue? = nil,
        typeOfError: E.Type,
        completionHandler: @escaping (DataResponse<T>) -> Void)
        -> Self
    {
        return response(
            queue: queue,
            responseSerializer: DataRequest.modelSerializer(type: typeOfError),
            completionHandler: completionHandler
        )
    }
    
}
