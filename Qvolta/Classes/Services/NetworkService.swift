//
//  NetworkService.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 1/25/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import Foundation
import Alamofire
import ReactiveSwift
import ResponseDetective
import ObjectMapper

enum NetworkRouter {
    case configuration
    
    case mobileAuth(login: String, password: String)
    case oAuth
    
    case signUp(username: String, email: String, password: String)
    
    case user
}

extension NetworkRouter: URLRequestConvertible {
    
    static let baseURL = URL(string: "http://qvolta-api.eu-central-1.elasticbeanstalk.com/")!
    static let apiPrefix = "api"
    static let publicPrefix = "public"
    
    static let basicHeaders = ["Content-Type" : "application/json",
                               "Accept"       : "application/json"]

    struct Path {
        static let SignIn = ""
        static let User = "user"
    }
    
    var method: HTTPMethod {
        switch self {
        case .oAuth, .mobileAuth, .signUp:
            return .post
        case .user, .configuration:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .configuration:
            return NetworkRouter.publicPrefix + "/configuration"
        case .mobileAuth:
            return "mobile/get-credentials"
        case .oAuth:
            return "oauth/token"
        case .signUp:
            return "register"
        case .user:
            return NetworkRouter.apiPrefix + "/user"
        }
    }
    
    var headers: [String: String]? {
        switch self {
        case .mobileAuth(let login, _):
            var headers = NetworkRouter.basicHeaders
            
            let nonce = String(UUID().uuidString.md5!.prefix(16))
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
            let dateString = dateFormatter.string(from: Date())
            let digest = Data("\(nonce)\(dateString)\(Keys.ClientSecret)".utf8).SHA1().base64EncodedString()
            headers["x-wsse"] = "UsernameToken Username=\"\(login)\", PasswordDigest=\"\(digest)\", Nonce=\"\(nonce.toBase64())\", Created=\"\(dateString)\""
            
            return headers
        default:
            return NetworkRouter.basicHeaders
        }
    }
    
    var parametersGET: [String: String]? {
        switch self {
        default:
            return nil
        }
    }
    
    var parametersJSON: String? {
        switch self {
        case .mobileAuth(_, let password):
            return LoginRequest(password: password).toJSONString()
        case .signUp(let username, let email, let password):
            return UserCreateRequest(username: username, email: email, password: password).toJSONString()
        default:
            return nil
        }
    }
    
    var fullPathURL: URL {
        return NetworkRouter.baseURL.appendingPathComponent(path)
    }
    
    
    func asURLRequest() throws -> URLRequest {
        var request = try URLRequest(url: fullPathURL, method: method)
        
        if let _ = parametersJSON, method != .get {
            request.httpBody = parametersJSON?.data(using: .utf8)
        }
        
        if let parametersGET = parametersGET {
            var queryItems: [URLQueryItem] = []
            
            for (key, value) in parametersGET {
                queryItems.append(URLQueryItem(name: key, value: value))
            }
            
            if queryItems.count > 0 {
                var urlComponents = URLComponents(string: (request.url?.absoluteString)!)!
                urlComponents.queryItems = queryItems as [URLQueryItem]
                request.url = urlComponents.url
            }
        }
        
        if let headers = self.headers {
            for (header, value) in headers {
                request.setValue(value, forHTTPHeaderField: header)
            }
        }
        
        return request
    }
}



protocol NetworkService {
    
    // MARK: System
    func set(token: Token?)
    func checkReachability(_ status: NetworkReachabilityManager.NetworkReachabilityStatus) -> Bool
    
    // MARK: Requests
    func getConfiguration() -> SignalProducer<ResponseArray<Configuration>, APIError>
    func getTokenBy(login: String, password: String) -> SignalProducer<Token, APIError>
    
    func postUser(username: String, email: String, password: String) -> SignalProducer<Response, APIError>
    
    func getUser() -> SignalProducer<User, APIError>
}


class NetworkImplementation: NetworkService {
    
    var isReachable: MutableProperty<Bool> = MutableProperty(true)
    
    var sessionManager: SessionManager!
    let reachabilityManager = NetworkReachabilityManager(host: "www.apple.com")!
    
    init() {
        let configuration = URLSessionConfiguration.default
        ResponseDetective.enable(inConfiguration: configuration)
        sessionManager = Alamofire.SessionManager(configuration: configuration)
        
        reachabilityManager.listener = { [weak self] status in
            guard let `self` = self else { return }
            self.isReachable.value = self.checkReachability(status)
        }
        
        reachabilityManager.startListening()
        self.isReachable.value = reachabilityManager.isReachable
    }
    
    func checkReachability(_ status: NetworkReachabilityManager.NetworkReachabilityStatus) -> Bool {
        switch status {
        case .unknown:
            return false
        case .notReachable:
            return false
        default:
            return true
        }
    }
    
    func set(token: Token?) {
        if let token = token {
            let handler = NetworkOAuth2Handler(accessToken: token.access_token,
                                               refreshToken: token.refresh_token)
            sessionManager.adapter = handler
            sessionManager.retrier = handler
        } else {
            sessionManager.adapter = nil
            sessionManager.retrier = nil
        }
    }
}

extension NetworkImplementation {
    
    func getConfiguration() -> SignalProducer<ResponseArray<Configuration>, APIError> {
        return makeRequestTo(.configuration)
    }
    
    func getTokenBy(login: String, password: String) -> SignalProducer<Token, APIError> {
        return makeRequestTo(.mobileAuth(login: login, password: password))
    }
    
    func postUser(username: String, email: String, password: String) -> SignalProducer<Response, APIError> {
        return makeRequestTo(.signUp(username: username, email: email, password: password))
    }
    
    func getUser() -> SignalProducer<User, APIError> {
        return makeRequestTo(.user)
    }
}


private extension NetworkImplementation {
    func makeRequestTo<T: Mappable>(_ route: NetworkRouter) -> SignalProducer<T, APIError> {
        
        return SignalProducer<T, APIError> { observer, disposable in
            self.sessionManager
                .request(route)
                .validate()
                .responseModel(typeOfError: APIError.self) { (response: DataResponse<T>) in
                    switch response.result {
                    case .success(let value):
                        observer.send(value: value)
                        break
                    case .failure(let error):
                        observer.send(error: APIError(error))
                        break
                    }
                    
                    observer.sendCompleted()
            }
        }
    }
}

// MARK: - Request handlers
private class NetworkOAuth2Handler: RequestAdapter, RequestRetrier {
    private var accessToken: String
    private var refreshToken: String
    
    private let lock = NSLock()
    
    private typealias RefreshCompletion = (_ succeeded: Bool, _ accessToken: String?, _ refreshToken: String?) -> Void
    
    private var isRefreshing = false
    private var requestsToRetry: [RequestRetryCompletion] = []
    
    private let sessionManager: SessionManager = {
        let configuration = URLSessionConfiguration.default
        ResponseDetective.enable(inConfiguration: configuration)
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        
        return SessionManager(configuration: configuration)
    }()
    
    init(accessToken: String, refreshToken: String) {
        self.accessToken = accessToken
        self.refreshToken = refreshToken
    }
    
    deinit {
        self.requestsToRetry.removeAll()
    }
    
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        var urlRequest = urlRequest
    
        // All API methods but not oAuth
        let checkPrefix = NetworkRouter.baseURL.absoluteString + NetworkRouter.apiPrefix
        
        if let urlString = urlRequest.url?.absoluteString, urlString.hasPrefix(checkPrefix) {
            urlRequest.setValue("Bearer " + accessToken, forHTTPHeaderField: "Authorization")
        }
        
        return urlRequest
    }
    
    // MARK: - RequestRetrier
    
    func should(_ manager: SessionManager,
                retry request: Request,
                with error: Error,
                completion: @escaping RequestRetryCompletion) {
        lock.lock() ; defer { lock.unlock() }
        
        if let response = request.task?.response as? HTTPURLResponse, response.statusCode == 401 {
            requestsToRetry.append(completion)
            
            if !isRefreshing {
                refreshTokens { [weak self] succeeded, accessToken, refreshToken in
                    guard let `self` = self else { return }
                    
                    self.lock.lock() ; defer { self.lock.unlock() }
                    
                    if let accessToken = accessToken, let refreshToken = refreshToken {
                        self.accessToken = accessToken
                        self.refreshToken = refreshToken
                    }
                    
                    self.requestsToRetry.forEach { $0(succeeded, 0.0) }
                    self.requestsToRetry.removeAll()
                }
            }
        } else {
            completion(false, 0.0)
        }
    }
    
    // MARK: - Private - Refresh Tokens
    
    private func refreshTokens(completion: @escaping RefreshCompletion) {
        guard !isRefreshing else { return }
        
        isRefreshing = true
        
//        let token = RefreshTokenRequest(refreshToken)
//
//        sessionManager
//            .request(NetworkRouter.oauth(token))
//            .responseModel(typeOfError: APIError.self) { [weak self] (response: DataResponse<Token>) in
//                switch response.result {
//                case .success(let value):
//                    AppSessionManager.shared.token = value
//                    completion(true, value.access_token, value.refresh_token)
//                    break
//                case .failure(_):
//                    completion(false, nil, nil)
//                    break
//                }
//
//                self?.isRefreshing = false
//        }
    }
}
