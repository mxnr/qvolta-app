//
//  Session.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/2/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import Foundation

class Session {
    
    var token: Token?
    var configuration: Configuration!
    
    // MARK: - Singleton
    static let shared = Session()
    private init() {}
    
    
}
