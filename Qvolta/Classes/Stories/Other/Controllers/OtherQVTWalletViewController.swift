//
//  OtherQVTWalletViewController.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/23/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import UIKit

import ReactiveCocoa
import ReactiveSwift

class OtherQVTWalletViewController: UIViewController {

    @IBOutlet weak var backButton: UIButton!

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var balanceTitleLabel: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!
    
    @IBOutlet weak var addressTitleLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    @IBOutlet weak var copyButton: UIButton!
    
    @IBOutlet weak var qrTitleLabel: UILabel!
    @IBOutlet weak var qrImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        theme()
    }
    
    func theme() {
        view.backgroundColor = .gray4
        backButton.reactive.controlEvents(.touchUpInside).observe { [weak self] (signal) in
            self?.dismiss(animated: true, completion: nil)
        }
        
        titleLabel.textColor = .gray2
        balanceTitleLabel.textColor = .gray1
        balanceLabel.textColor = .brandActive
        
        addressTitleLabel.textColor = .gray2
        addressLabel.textColor = .gray1
        
        qrTitleLabel.textColor = .gray2
        
        copyButton.layer.cornerRadius = 8.0
        copyButton.layer.borderWidth = 1.0
        copyButton.layer.borderColor = UIColor.blueBlue.cgColor
        copyButton.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        copyButton.setTitleColor(.blueBlue, for: .normal)
        copyButton.setTitleColor(UIColor.blueBlue.withAlphaComponent(0.5),
                                        for: .highlighted)
        copyButton.setTitleColor(.blueBlue, for: .selected)
    }
    
    @IBAction func buttonCopyTouched(_ sender: UIButton) {
        UIPasteboard.general.string = ""
        sender.isSelected = true
    }
}
