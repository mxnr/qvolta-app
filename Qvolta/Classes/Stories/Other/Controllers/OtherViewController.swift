//
//  OtherViewController.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/2/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import UIKit

class OtherViewController: UIViewController {
    
    @IBOutlet weak var stackView: UIStackView!
    
    @IBOutlet var chevronsArray: [UIImageView]!
    
    @IBOutlet var buttonsArray: [UIButton]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        theme()
    }
    
    func theme() {
        view.backgroundColor = .gray4
        chevronsArray.forEach { (imageView) in
            imageView.transform = CGAffineTransform(rotationAngle: -CGFloat(Double.pi / 2))
        }
        
        buttonsArray.forEach { (button) in
            button.setTitleColor(.gray1, for: .normal)
            button.setTitleColor(UIColor.gray1.withAlphaComponent(0.6), for: .highlighted)
            button.layer.cornerRadius = 6.0
            button.backgroundColor = .white
        }
        
    }
}
