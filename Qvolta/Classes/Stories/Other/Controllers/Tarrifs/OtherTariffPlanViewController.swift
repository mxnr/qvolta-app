//
//  OtherTariffPlanViewController.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/23/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import UIKit

class OtherTariffPlanViewController: UIViewController {
    
    var viewModel: OtherTariffPlanViewModel! = OtherTariffPlanViewModel()

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var selectValueTitleLabel: UILabel!
    @IBOutlet weak var selectValueTextField: UITextField!
    
    @IBOutlet weak var plansContainerView: UIView!
    @IBOutlet weak var plansTitleLabel: UILabel!
    
    @IBOutlet weak var plansTableView: UITableView!
    
    @IBOutlet weak var buyButton: GradientButton!
    
    let pickerView: UIPickerView = UIPickerView()
    
    var textFieldCoinImageView: UIImageView = UIImageView()
    
    // MARK: - Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()

        pickerView.dataSource = self
        pickerView.delegate = self
        
        plansContainerView.alpha = 0.0
        buyButton.alpha = 0.0
        
        theme()
        setupReactive()
    }
    
    func setupReactive() {
        viewModel.selectedCoin.signal.observeValues { [weak self] (coin) in
            guard let coin = coin else { return }
            
            self?.textFieldCoinImageView.image = coin.settings.logoSmall
            self?.selectValueTextField.text = coin.settings.name
            
            self?.plansTableView.reloadData()
            
            if self?.plansContainerView.alpha == 0 {
                UIView.animate(withDuration: 0.2) {
                    self?.plansContainerView.alpha = 1.0
                }
            }
            
            if self?.buyButton.alpha == 1 {
                UIView.animate(withDuration: 0.2) {
                    self?.buyButton.alpha = 0.0
                }
            }
        }
        
        backButton.reactive.controlEvents(.touchUpInside).observe { [weak self] (signal) in
            self?.dismiss(animated: true, completion: nil)
        }
    }
    
    func theme() {
        view.backgroundColor = .gray4
        titleLabel.textColor = .gray2
        
        selectValueTitleLabel.textColor = .gray2
        plansTitleLabel.textColor = .gray2
        
        buyButton.setupWithColors(left: .blueBlue, right: .turquoise, shadow: .turquoise)
        
        selectValueTextField.inputAccessoryView = UIToolbar.withButton(tintColor: .turquoise,
                                                                       target: self,
                                                                       selector: #selector(didSelectValue),
                                                                       title: "Done")
        selectValueTextField.inputView = pickerView
        selectValueTextField.layer.borderColor = UIColor.gray3.cgColor
        selectValueTextField.layer.borderWidth = 1.0
        selectValueTextField.layer.cornerRadius = 6.0
        
        selectValueTextField.backgroundColor = .white
        
        selectValueTextField.delegate = self
        
        selectValueTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: selectValueTextField.frame.size.height + 5, height: selectValueTextField.frame.size.height))
        selectValueTextField.leftViewMode = .always
        selectValueTextField.tintColor = .clear
        selectValueTextField.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        
        textFieldCoinImageView.contentMode = .scaleAspectFit
        selectValueTextField.leftView?.addSubview(textFieldCoinImageView)
        textFieldCoinImageView.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-5)
            make.width.height.equalTo(30)
        }
    }
    
    @objc func didSelectValue() {
        view.endEditing(true)
        
        viewModel.selectedCoin.value = viewModel.coins[pickerView.selectedRow(inComponent: 0)]
    }
}

extension OtherTariffPlanViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard buyButton.alpha == 0 else { return }
        
        UIView.animate(withDuration: 0.3) {
            self.buyButton.alpha = 1.0
        }
    }
}

extension OtherTariffPlanViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (viewModel.selectedCoin.value != nil) ? viewModel.plans.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OtherTariffOption") as! OtherTariffOptionTableViewCell
        
        let string = viewModel.plans[indexPath.row]
        
        if let coin = viewModel.selectedCoin.value {
            cell.coin = coin
        }
        
        cell.selectionButton.setTitle(string, for: .normal)
        
        return cell
    }
}

extension OtherTariffPlanViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return false
    }
}

extension OtherTariffPlanViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return viewModel.coins.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return viewModel.coins[row].settings.name
    }
}
