//
//  OtherTariffPlanViewModel.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/23/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import Foundation
import ReactiveCocoa
import ReactiveSwift


class OtherTariffPlanViewModel {
    var coins: [Coin] = [.BTC, .ETH]
    
    var plans: [String]!
    
    var selectedCoin: MutableProperty<Coin?> = MutableProperty(nil)
    
    init() {
        plans = ["Up to 5 BTC — 5.5 QVT", "Up to 25 BTC — 22.5 QVT", "Up to 50 BTC — 40 QVT"]
    }
    
}
