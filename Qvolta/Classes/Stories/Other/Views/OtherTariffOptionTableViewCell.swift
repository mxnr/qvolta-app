//
//  OtherTariffOptionTableViewCell.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/23/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import UIKit

class OtherTariffOptionTableViewCell: UITableViewCell {

    @IBOutlet weak var selectionButton: GradientButton!
    
    var coin: Coin?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionButton.setTitleColor(.gray1, for: .normal)
        selectionButton.setTitleColor(.white, for: .selected)
        
        let font = UIFont.systemFont(ofSize: 14, weight: .regular)
        selectionButton.titleLabel?.font = font
        selectionButton.layer.cornerRadius = 6
        
        selectionButton.gradientLayer.cornerRadius = 6
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        guard let coin = coin else { return }
        
        let settings = coin.settings
        
        if selected == true {
            selectionButton.layer.borderWidth = 0
            selectionButton.setupWithColors(left: settings.firstColor,
                                            right: settings.secondColor,
                                            shadow: settings.shadowColor)
        } else {
            selectionButton.makePlain()
            selectionButton.backgroundColor = .white
            selectionButton.layer.borderWidth = 1
            selectionButton.layer.borderColor = settings.firstColor.cgColor
        }
        
        selectionButton.isSelected = selected
    }
    

}
