//
//  TradeTableViewCell.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/24/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import UIKit

class TradeTableViewCell: UITableViewCell {

    @IBOutlet weak var onlineIndicatorView: UIView!
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var priceCoinTitleLabel: UILabel!
    @IBOutlet weak var priceCoinLabel: UILabel!
    
    @IBOutlet weak var priceOrderTitleLabel: UILabel!
    @IBOutlet weak var priceOrderLabel: UILabel!
    
    @IBOutlet weak var paymentImageView: UIImageView!
    
    @IBOutlet weak var buySellButton: GradientButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        nameLabel.textColor = .gray1
        priceCoinTitleLabel.textColor  = .gray3
        priceOrderTitleLabel.textColor = .gray3
        priceCoinLabel.textColor  = .gray1
        priceOrderLabel.textColor = .gray1
        
        buySellButton.setupWithColors(left: .blueBlue,
                                      right: .turquoise,
                                      shadow: UIColor.turquoise.withAlphaComponent(0.6))

        buySellButton.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .medium)
    }

    
    func bind(_ order: TradeOrder) {
        nameLabel.text = order.user.username
        photoImageView.image = order.user.photoImage
        paymentImageView.image = order.paymentSystem.logo
        priceCoinLabel.text = "\(order.priceCoin!)"
        priceOrderLabel.text = "\(order.priceCurrency!)"
        
        buySellButton.setTitle(order.orderType.rawValue, for: .normal)
        
//        onlineIndicatorView.isHidden
        
        
    }

}
