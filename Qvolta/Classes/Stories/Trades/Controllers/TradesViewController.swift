//
//  TradesViewController.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/2/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import UIKit
import ReactiveSwift
import ReactiveCocoa

class TradesViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var sectionsSegmentedControl: UISegmentedControl!
    
    @IBOutlet weak var noTradesView: UIView!
    @IBOutlet weak var noTradesTitleLabel: UILabel!
    @IBOutlet weak var noTradesDescriptionLabel: UILabel!
    @IBOutlet weak var noTradesCreateOfferButton: BorderButton!
    
    @IBOutlet weak var numberOfTradesLabel: UILabel!
    
    @IBOutlet weak var tradesTableView: UITableView!
    
    var viewModel: TradesViewModel = TradesViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tradesTableView.register(UINib.init(nibName: "TradeTableViewCell", bundle: nil), forCellReuseIdentifier: "TradeTableViewCell")
        
        theme()
        setupReactive()
    }
    
    func theme() {
        view.backgroundColor = .gray4
        tradesTableView.backgroundColor = .gray4
        tradesTableView.separatorColor = .gray4
        
        titleLabel.textColor = .gray1
        
        numberOfTradesLabel.textColor = .gray2
        
        sectionsSegmentedControl.setTitleTextAttributes([NSAttributedStringKey.font : UIFont.systemFont(ofSize: 14, weight: .regular)], for: .normal)
        sectionsSegmentedControl.tintColor = .brandActive
        
        noTradesView.backgroundColor = .gray4
        noTradesTitleLabel.textColor = .gray1
        noTradesDescriptionLabel.textColor = .gray2
        
        noTradesCreateOfferButton.setup(color: UIColor.brandActive)
    }
    
    func setupReactive() {
        viewModel.selectedSection <~ sectionsSegmentedControl.reactive.selectedSegmentIndexes
        
        viewModel.ordersArray.producer.startWithValues { [weak self] (orders) in
            self?.tradesTableView.reloadData()
            
            let count = orders.count
            
            self?.numberOfTradesLabel.text = String(format: NSLocalizedString("ActiveTrades", comment: ""), count)
            
            self?.noTradesView.isHidden = count > 0
        }
    }
}

extension TradesViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let order = viewModel.ordersArray.value[indexPath.row]
        
        let controller: TradeOrderDetailsViewController = storyboard?.instantiateViewController(withIdentifier: "TradeOrderDetailsViewController") as! TradeOrderDetailsViewController
        controller.viewModel = TradeOrderDetailsViewModel(with: order)
        
        present(controller, animated: true)
    }
    
}

extension TradesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.ordersArray.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TradeTableViewCell") as! TradeTableViewCell
        
        let order = viewModel.ordersArray.value[indexPath.row]
        cell.bind(order)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 1))
        view.backgroundColor = .gray4
        
        return view
    }
    
}
