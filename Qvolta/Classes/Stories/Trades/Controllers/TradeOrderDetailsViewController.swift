//
//  TradeOrderDetailsViewController.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/24/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import UIKit

class TradeOrderDetailsViewController: UIViewController {
    
    @IBOutlet weak var topNavigationView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var detailsContainerView: UIView!
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var priceTitleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var orderTitleLabel: UILabel!
    @IBOutlet weak var orderLabel: UILabel!
    
    @IBOutlet weak var paymentLabel: UILabel!
    
    @IBOutlet weak var textFieldsContainerView: UIView!
    @IBOutlet weak var transactionAmountTitleLabel: UILabel!
    @IBOutlet weak var amountCurrencyTextField: UITextField!
    @IBOutlet weak var amountCoinTextField: UITextField!
    @IBOutlet weak var amountCoinImageView: UIImageView!
    
    @IBOutlet weak var sendRequestButton: GradientButton!
    
    var viewModel: TradeOrderDetailsViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        theme()
        setup()
    }

    func theme() {
        titleLabel.textColor = .gray2
        view.backgroundColor = .gray4
        descriptionLabel.textColor = .gray3
        
        [priceTitleLabel, orderTitleLabel, paymentLabel, transactionAmountTitleLabel]
            .forEach { (label) in
            label?.font = UIFont.systemFont(ofSize: 14, weight: .regular)
            label?.textColor = .gray2
        }
        
        [nameLabel, priceLabel, orderLabel].forEach { (label) in
            label?.textColor = .gray1
        }
        
        backButton.reactive.controlEvents(.touchUpInside).observe { [weak self] (signal) in
            self?.dismiss(animated: true, completion: nil)
        }
        
        let viewsForShadow = [topNavigationView, detailsContainerView, textFieldsContainerView]
        viewsForShadow.forEach { (view) in
            view?.layer.shadowOffset = CGSize(width: 0, height: 6)
            view?.layer.shadowRadius = 10
            view?.layer.shadowColor = UIColor.lightBlueGrey.cgColor
            view?.layer.shadowOpacity = 0.5
            
            view?.layer.cornerRadius = 6.0
        }
        
        sendRequestButton.setupWithColors(left: .blueBlue,
                                          right: .turquoise,
                                          shadow: .turquoise)
        
        let textFieldsArray: [UITextField] = [amountCurrencyTextField, amountCoinTextField]
        
        textFieldsArray.forEach { (textField) in
            textField.delegate = self
            
            textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 1))
            textField.leftViewMode = .always
            textField.font = UIFont.systemFont(ofSize: 14, weight: .regular)
            textField.textColor = .gray1
            
            textField.layer.borderColor = UIColor.gray3.cgColor
            textField.layer.borderWidth = 1.0
            textField.layer.cornerRadius = 6.0
            
            textField.backgroundColor = .white
        }
    }
    
    func setup() {
        nameLabel.text = viewModel.order.user.username
        
        priceLabel.text = "\(viewModel.order.priceCoin!)"
        orderLabel.text = "\(viewModel.order.priceCurrency!)"
        
        amountCoinImageView.image = viewModel.order.coin.settings.logoSmall
    }
}

extension TradeOrderDetailsViewController: UITextFieldDelegate {
    
}
