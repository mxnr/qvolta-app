//
//  TradesViewModel.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/24/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import Foundation
import ReactiveCocoa
import ReactiveSwift

class TradesViewModel {
    enum Section: Int {
        case Sell
        case Purchase
    }
    
    var selectedSection: MutableProperty<Int> = MutableProperty(0)
    var ordersArray: MutableProperty<[TradeOrder]> = MutableProperty([])
    
    init() {
        setupReactive()
    }
    
    func setupReactive() {
        selectedSection.producer.startWithValues { [weak self] section in
            switch Section(rawValue: section)! {
            case .Sell:
                self?.setSellOrders()
                break;
            case .Purchase:
                self?.setPurchaseOrders()
                break;
            }
        }
    }
    
    func setPurchaseOrders() {
        var user = User(id: 2, username: "Phillip Sanders")
        user.isVerified = true
        let payment = Session.shared.configuration.paymentMethods.first!
        let order = TradeOrder(user: user,
                               paymentSystem: payment,
                               priceCoin: 4560000,
                               priceCurrency: 14560000,
                               orderType: TradeOrder.OrderType.Buy,
                               coin: .BTC)
        let order2 = TradeOrder(user: User(id: 3, username: "Dustin Blair"),
                               paymentSystem: payment,
                               priceCoin: 4560000,
                               priceCurrency: 14560000,
                               orderType: TradeOrder.OrderType.Buy,
                               coin: .ETH)
        
        ordersArray.value = [order, order2]
    }
    
    func setSellOrders() {
        var user = User(id: 2, username: "2Phillip Sanders")
        user.isVerified = true
        let payment = Session.shared.configuration.paymentMethods.first!
        let order = TradeOrder(user: user,
                               paymentSystem: payment,
                               priceCoin: 4560000,
                               priceCurrency: 14560000,
                               orderType: TradeOrder.OrderType.Sell,
                               coin: .BTC)
        
        ordersArray.value = [order]
    }
}
