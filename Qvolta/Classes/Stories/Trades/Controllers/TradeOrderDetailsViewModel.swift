//
//  TradeOrderDetailsViewModel.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/25/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import Foundation

class TradeOrderDetailsViewModel {
    var order: TradeOrder!
    
    init(with order: TradeOrder) {
        self.order = order
    }
}
