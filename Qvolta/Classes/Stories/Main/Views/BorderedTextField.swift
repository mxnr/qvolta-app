//
//  BorderedTextField.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 3/4/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import UIKit
import SnapKit

@IBDesignable class BorderedTextField: UITextField {

    private var imageView: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    func setup() {
        layer.cornerRadius = 6.0
        
        leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 1))
        leftViewMode = .always
        font = UIFont.systemFont(ofSize: 14, weight: .regular)
        
        layer.borderWidth = 1.0
        layer.borderColor = UIColor.gray3.cgColor
        backgroundColor = .white

        imageView = UIImageView()
        addSubview(imageView)
        imageView.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.width.height.equalTo(30)
            make.trailing.equalToSuperview().offset(-10)
        }
    }
    
    var image: UIImage? {
        get {
            return imageView.image
        }
        set {
            imageView.image = image
        }
    }

}
