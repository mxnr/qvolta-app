//
//  QVTextField.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/1/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import UIKit
import SnapKit

class SignUpTextField: UITextField {
    
    private var errorImageView: UIImageView!

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        layer.cornerRadius = 6.0
        
        leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 1))
        leftViewMode = .always
        font = UIFont.systemFont(ofSize: 14, weight: .regular)
        
        layer.borderWidth = 1.0
        layer.borderColor = UIColor.clear.cgColor
        
        errorImageView = UIImageView(image: #imageLiteral(resourceName: "icValidationFailed"))
        addSubview(errorImageView)
        errorImageView.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview().offset(-10)
        }
        errorImageView.isHidden = true
    }
    
    enum State {
        case Normal
        case Active
        case Error
    }
    
    func set(state: State) {
        switch state {
        case .Normal:
            errorImageView.isHidden = true
            layer.borderColor = UIColor.clear.cgColor
        case .Active:
            errorImageView.isHidden = true
            layer.borderColor = UIColor.brandActive.cgColor
        case .Error:
            errorImageView.isHidden = false
            layer.borderColor = UIColor.systemRed.cgColor
        }
    }

}
