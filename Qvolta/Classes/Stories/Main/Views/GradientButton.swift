//
//  GradientButton.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 1/31/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import UIKit

import SnapKit

class GradientButton: UIButton {
    
    var gradientLayer: CAGradientLayer = CAGradientLayer()
    
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
   
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    var cornerRadius: CGFloat {
        get { return layer.cornerRadius }
        
        set {
            layer.cornerRadius = newValue
            gradientLayer.cornerRadius = newValue
        }
    }
    
    private func setup() {
        layer.cornerRadius = 8.0
        let font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        titleLabel?.font = font
        setTitleColor(.white, for: .normal)
        setTitleColor(UIColor(white: 1.0, alpha: 0.5), for: .disabled)
        
        gradientLayer.frame = bounds
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
        gradientLayer.cornerRadius = layer.cornerRadius
        
        layer.insertSublayer(gradientLayer, below: titleLabel?.layer)
        
        activityIndicator.hidesWhenStopped = true
        
        addSubview(activityIndicator)
        activityIndicator.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview().offset(-20)
        }
        activityIndicator.stopAnimating()
    }
    
    
    func setupWithColors(left: UIColor, right: UIColor, shadow: UIColor?) {
        gradientLayer.isHidden = false
        gradientLayer.colors = [left.cgColor, right.cgColor]
        gradientLayer.locations = [0.0, 1.0]
        
        guard let shadow = shadow else { return }
        
        layer.shadowColor = shadow.cgColor
        layer.shadowOpacity = 0.7
        layer.shadowOffset = CGSize(width: 0, height: 10)
        layer.shadowRadius = 20
    }
    
    func makePlain() {
        gradientLayer.isHidden = true
        layer.shadowOpacity = 0.0
    }
    
    private var iconImageView: UIImageView?
    
    func addIcon(_ icon: UIImage) {
        if iconImageView != nil {
            iconImageView?.removeFromSuperview()
            iconImageView = nil
        }
        
        iconImageView = UIImageView(image: icon)
        addSubview(iconImageView!)
        iconImageView!.snp.makeConstraints { (make) in
            make.top.left.bottom.equalToSuperview().inset(UIEdgeInsetsMake(13, 15, 12, 0))
            make.width.height.equalTo(35)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        gradientLayer.frame = bounds
        layer.shadowPath = CGPath(rect: bounds.insetBy(dx: 10, dy: 0), transform: nil)
    }
    
    override var isHighlighted: Bool {
        didSet {
            alpha = (isHighlighted) ? 0.5 : 1.0
        }
    }
    
}
