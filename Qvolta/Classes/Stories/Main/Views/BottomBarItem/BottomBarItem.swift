//
//  BottomBarItem.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/2/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import UIKit

@IBDesignable class BottomBarItem: UIControl {
    
    @IBInspectable var normalImage: UIImage! {
        didSet {
            if !isSelected {
                iconImageView.image = normalImage
            }
        }
    }
    
    @IBInspectable var selectedImage: UIImage! {
        didSet {
            if isSelected {
                iconImageView.image = selectedImage
            }
        }
    }
    
    @IBInspectable var isActive: Bool {
        set {
            isSelected = newValue
        }
        get {
            return isSelected
        }
    }
    
    override var isSelected: Bool {
        didSet {
            setIsSelected(isSelected)
        }
    }
    
    @IBInspectable var title: String = "" {
        didSet {
            titleLabel.text = title
        }
    }
    
    private var iconImageView: UIImageView! = UIImageView()
    
    private var titleLabel: UILabel! = UILabel()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        
        layoutIfNeeded()
    }

    func setup() {
        titleLabel.textAlignment = .center
        iconImageView.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(iconImageView)
        addSubview(titleLabel)
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-6-[imageView(30)]-2-[titleLabel]",
                                                     metrics: nil,
                                                     views: ["imageView" : iconImageView,
                                                             "titleLabel" : titleLabel]))
        addConstraint(NSLayoutConstraint(item: iconImageView,
                                         attribute: .centerX,
                                         relatedBy: .equal,
                                         toItem: self,
                                         attribute: .centerX,
                                         multiplier: 1.0,
                                         constant: 0.0))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "[imageView(30)]",
                                                      metrics: nil,
                                                      views: ["imageView" : iconImageView]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[titleLabel]|",
                                                      metrics: nil,
                                                      views: ["titleLabel" : titleLabel]))
        
        titleLabel.font = UIFont.systemFont(ofSize: 10, weight: .regular)
        titleLabel.text = title
        isSelected = false
    }
    
    private func setIsSelected(_ selected: Bool) {
        iconImageView.image = (selected) ? selectedImage : normalImage
        titleLabel.textColor = (selected) ? .brandActive : .gray3
    }
    

}
