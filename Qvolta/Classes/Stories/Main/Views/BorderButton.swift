//
//  BorderButton.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/26/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import UIKit

class BorderButton: UIButton {

    func setup(color: UIColor,
               title: String? = nil,
               font: UIFont = UIFont.systemFont(ofSize: 14, weight: .semibold),
               cornerRadius: CGFloat = 8) {
        layer.cornerRadius = cornerRadius
        layer.borderWidth = 1.0
        layer.borderColor = color.cgColor
        titleLabel?.font = font
        
        if let title = title {
            setTitle(title, for: .normal)
        }
        
        setTitleColor(color, for: .normal)
        setTitleColor(color.withAlphaComponent(0.5),
                      for: .highlighted)
        setTitleColor(color, for: .selected)
    }

}
