//
//  UnderlineButton.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/10/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import UIKit

@IBDesignable class UnderlineButton: UIButton {
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        if let context = UIGraphicsGetCurrentContext(), isSelected {
            context.setStrokeColor(titleColor(for: .selected)!.cgColor)
            context.setLineWidth(2.0)
            context.move(to: CGPoint(x: 0, y: rect.maxY - 1))
            context.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY - 1))
            context.strokePath()
        }
    }

}
