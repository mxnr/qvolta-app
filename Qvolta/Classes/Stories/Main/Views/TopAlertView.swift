//
//  TopAlertView.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/1/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import UIKit
import SnapKit
import ReactiveSwift

class TopAlertView: UIWindow {
    
    private var alertView: InternalTopAlertView!
    private var timer: Timer?
    
    var isShown: MutableProperty<Bool>! = MutableProperty(false)
    var isClosing: Bool = false

    static func show(title: String, backgroundColor: UIColor) -> TopAlertView {
        let window = TopAlertView(frame: UIScreen.main.bounds)
        window.backgroundColor = .clear
        
        window.alertView = InternalTopAlertView(frame: CGRect.zero)
        let heightWithStatusBar = CGFloat(InternalTopAlertView.internalHeight) + UIApplication.shared.statusBarFrame.size.height
        window.addSubview(window.alertView)
        window.alertView.snp.makeConstraints { (make) in
            make.left.right.top.equalToSuperview()
            make.height.equalTo(heightWithStatusBar)
        }
        
        window.alertView.setup(text: title, background: backgroundColor)
        window.alertView.transform = CGAffineTransform(translationX: 0, y: -heightWithStatusBar)
        
        window.alertView.closeButton.addTarget(window, action: #selector(didCloseTouch), for: .touchUpInside)
        
        window.isHidden = false
        
        window.show()
        
        return window
    }
    
    func show() {
        UIApplication.shared.statusBarStyle = .lightContent
        isShown.value = true

        timer = Timer.scheduledTimer(timeInterval: 3,
                                     target: self,
                                     selector: #selector(didCloseTouch),
                                     userInfo: nil,
                                     repeats: false)
        
        UIView.animate(withDuration: 0.3) {
            self.alertView.transform = CGAffineTransform.identity
        }
    }
    
    func hide() {
        didCloseTouch()
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        guard alertView.frame.contains(point) else { return nil }
        
        return alertView
    }
    
    @objc func didCloseTouch() {
        UIApplication.shared.statusBarStyle = .default
        
        if let timer = timer {
            timer.invalidate()
        }

        guard isClosing == false else { return }
        
        isShown.value = false
        isClosing = true
        
        UIView.animate(withDuration: 0.3,
                       animations: {
                        self.alertView.transform = CGAffineTransform(translationX: 0, y: -self.alertView.frame.size.height)
        }) { (completed) in
            self.window?.isHidden = true
            self.isClosing = false
        }
    }
}

private class InternalTopAlertView: UIView {
    var label: UILabel = UILabel()
    var closeButton: UIButton!
    var containerView: UIView = UIView()
    
    static let internalHeight = 77
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        containerView.backgroundColor = .clear
        addSubview(containerView)
        containerView.snp.makeConstraints { (make) in
            make.height.equalTo(InternalTopAlertView.internalHeight)
            make.left.right.bottom.equalToSuperview()
        }
        
        closeButton = UIButton(type: .custom)
        closeButton.setImage(#imageLiteral(resourceName: "icClose"), for: .normal)
        closeButton.setTitle(nil, for: .normal)
        containerView.addSubview(closeButton)
        closeButton.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-20)
            make.width.height.equalTo(21)
        }
        
        label.backgroundColor = .clear
        label.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        label.textColor = .white
        label.numberOfLines = 2
        containerView.addSubview(label)
        label.snp.makeConstraints { (make) in
            make.left.bottom.top.equalToSuperview().inset(UIEdgeInsetsMake(15, 20, 15, 0))
            make.right.equalTo(closeButton.snp.left).offset(-20)
        }
    }
    
    func setup(text: String, background: UIColor) {
        backgroundColor = background
        label.text = text
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
