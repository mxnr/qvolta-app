//
//  ViewController.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 1/10/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import UIKit

class EntryPointViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let network: NetworkService = ServiceLocator.shared.get()!
        
        network.getConfiguration().startWithResult { [weak self] (result) in
            switch result {
            case .success(let result):
                Session.shared.configuration = result.data!
                self?.showLogin()
                
                break
            case .failure(let error):
                print(error)
                self?.showLogin()
                
                break
            }
        }
        
        
    }
    
    func showLogin() {
        
        let controller: LoginViewController = UIStoryboard(name: "Unauthorized", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        
        //        let controller: AuthorizedTabBarViewController = storyboard!.instantiateViewController(withIdentifier: "AuthorizedTabBarViewController") as! AuthorizedTabBarViewController
        //
        self.navigationController?.setViewControllers([controller], animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

