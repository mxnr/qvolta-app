//
//  AuthorizedTabBarViewController.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/2/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import UIKit

class AuthorizedTabBarViewController: UIViewController {
    
    private var viewControllers: [UIViewController] = []
    
    @IBOutlet var bottomItems: [BottomBarItem]!
    @IBOutlet var contentView: UIView!
    
    weak var currentViewController: UIViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
    
        let walletController = UIStoryboard(name: "Wallet", bundle: nil).instantiateViewController(withIdentifier: "WalletViewController")
        let buySellController = UIStoryboard(name: "BuySell", bundle: nil).instantiateViewController(withIdentifier: "BuySellViewController")
        let tradesController = UIStoryboard(name: "Trades", bundle: nil).instantiateViewController(withIdentifier: "TradesViewController")
        let otherController = UIStoryboard(name: "Other", bundle: nil).instantiateViewController(withIdentifier: "OtherViewController")
        
        viewControllers = [walletController, buySellController, tradesController, otherController]
        
        showViewController(walletController)
    }
    
    @IBAction func bottomItemTouched(_ sender: BottomBarItem) {
        guard !sender.isSelected else { return }
        
        let index = bottomItems.index(of: sender)!
        
        showViewController(viewControllers[index])
        
        bottomItems.forEach { (item) in
            item.isSelected = item == sender
        }
    }
    
    func showViewController(_ viewController: UIViewController) {
        if let previousController = currentViewController {
            previousController.willMove(toParentViewController: nil)
            previousController.view.removeFromSuperview()
            previousController.removeFromParentViewController()
        }
        
        addChildViewController(viewController)
        viewController.view.frame = contentView.bounds
        contentView.addSubview(viewController.view)
        viewController.didMove(toParentViewController: self)
        
        currentViewController = viewController
    }
    

}
