//
//  LoginViewModel.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/1/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import Foundation
import ReactiveSwift
import ReactiveCocoa

class LoginViewModel {
    
    struct FormError: Error {
        let reason: String
        
        static let invalidEmail = FormError(reason: "E-mail is invalid.")
        static let invalidPassword = FormError(reason: "Password is invalid.")
    }
    
    var login: ValidatingProperty<String?, FormError> = ValidatingProperty(nil) { input in
        guard let login = input, login.count > 0, login.isValidEmail else { return .invalid(.invalidEmail) }
        
        return .valid
    }
    
    var password: ValidatingProperty<String?, FormError> = ValidatingProperty(nil) { input in
        guard let password = input, password.count > 0 else { return .invalid (.invalidPassword) }
        
        return .valid
    }
    
    
    var loginAction: Action<(), User, APIError>!

    init() {
        let validator = Property.combineLatest(login.result, password.result)
            .map { !$0.isInvalid && !$1.isInvalid }
        
        loginAction = Action(enabledIf: validator) { [weak self] in
            NetworkScenarios.logUserIn(User(email: (self?.login.value)!,
                                            password: (self?.password.value)!))
        }
    }
    
}
