//
//  SignUpViewModel.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/2/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import Foundation
import ReactiveSwift

class SignUpViewModel {
    struct FormError: Error {
        let reason: String
        
        static let invalid = FormError(reason: "Form is invalid.")
    }
    
    var email: ValidatingProperty<String?, FormError> = ValidatingProperty(nil) { input in
        guard let email = input, email.count > 0, email.isValidEmail else { return .invalid(.invalid) }
        
        return .valid
    }
    
    var username: ValidatingProperty<String?, FormError> = ValidatingProperty(nil) { input in
        guard let username = input, username.count > 0 else { return .invalid(.invalid) }
        
        return .valid
    }
    
    var password: ValidatingProperty<String?, FormError> = ValidatingProperty(nil) { input in
        guard let password = input, password.count > 5 else { return .invalid(.invalid) }
        
        return .valid
    }
    
    var confirmPassword: ValidatingProperty<String?, FormError>!
    var termsAccepted: MutableProperty<Bool> = MutableProperty(false)
    
    var registerAction: Action<(), User, APIError>!
    
    init() {
        confirmPassword = ValidatingProperty(nil, with: self.password) {
            input, password in
            guard let confirmPassword = input, password == confirmPassword else { return .invalid(.invalid) }
            
            return .valid
        }
        
        let validator = Property
            .combineLatest(email.result, username.result, password.result, confirmPassword.result, termsAccepted)
            .map { !$0.isInvalid && !$1.isInvalid && !$2.isInvalid && !$3.isInvalid && $4 }
        
        registerAction = Action(enabledIf: validator) { [weak self] () -> SignalProducer<User, APIError> in
            let network: NetworkService = ServiceLocator.shared.get()!
            
            return network
                .postUser(username: (self?.username.value)!,
                                    email: (self?.email.value)!,
                                    password: (self?.password.value)!)
            .flatMap(.latest, { (_) -> SignalProducer<User, APIError> in
                NetworkScenarios.logUserIn(User(email: (self?.email.value)!,
                                                password: (self?.password.value)!))
            })
        }
    }
    
}

