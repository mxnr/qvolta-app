//
//  ForgotPasswordViewController.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/2/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import UIKit
import ReactiveSwift
import ReactiveCocoa

class ForgotPasswordViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var emailTextField: SignUpTextField!
    @IBOutlet weak var requestButton: GradientButton!
    
    var viewModel: ForgotPasswordViewModel = ForgotPasswordViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.setNavigationBarHidden(false, animated: false)
        title = "Forgot Password"
        
        theme()
        setupReactive()
    }

    
    func theme() {
        view.backgroundColor = .gray4
        
        titleLabel.textColor = .gray2
        
        requestButton.layer.cornerRadius = 8.0
        requestButton.setupWithColors(left: .blueBlue, right: .turquoise, shadow: .turquoise)
    }
    
    func setupReactive() {
        viewModel.email <~ emailTextField.reactive.continuousTextValues
    }
    
}

extension ForgotPasswordViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        (textField as! SignUpTextField).set(state: .Active)
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        (textField as! SignUpTextField).set(state: .Normal)
    }
}
