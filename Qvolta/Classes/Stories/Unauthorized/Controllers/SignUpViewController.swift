//
//  SignUpViewController.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 1/28/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import UIKit
import ReactiveCocoa
import ReactiveSwift

class SignUpViewController: UIViewController {
    
    let viewModel: SignUpViewModel = SignUpViewModel()
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var usernameTextField: SignUpTextField!
    @IBOutlet weak var emailTextField: SignUpTextField!
    @IBOutlet weak var passwordTextField: SignUpTextField!
    @IBOutlet weak var confirmPasswordTextField: SignUpTextField!
    
    @IBOutlet var textFieldsCollection: [SignUpTextField]!
    
    @IBOutlet weak var checkBoxButton: UIButton!
    
    @IBOutlet weak var registerButton: GradientButton!
    
    @IBOutlet weak var agreeLabel: UILabel!
    
    @IBOutlet weak var termsAndConditionsButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.setNavigationBarHidden(false, animated: false)
        title = "Registration"
        
        theme()
        setupReactive()
    }
    
    func theme() {
        view.backgroundColor = .gray4
        
        agreeLabel.textColor = .niceBlue
        termsAndConditionsButton.tintColor = .niceBlue
        termsAndConditionsButton.setTitleColor(.niceBlue, for: .normal)
        
        registerButton.layer.cornerRadius = 8.0
        registerButton.setupWithColors(left: .blueBlue, right: .turquoise, shadow: .turquoise)
    }
    
    func setupReactive() {
        viewModel.username <~ usernameTextField.reactive.continuousTextValues
        viewModel.email <~ emailTextField.reactive.continuousTextValues
        viewModel.password <~ passwordTextField.reactive.continuousTextValues
        viewModel.confirmPassword <~ confirmPasswordTextField.reactive.continuousTextValues
        
        registerButton.reactive.pressed = CocoaAction(viewModel.registerAction)
        
        viewModel.registerAction.isExecuting.signal.observeValues { [weak self] (isExecuting) in
            self?.view.endEditing(true)
            
            if isExecuting == true {
                self?.registerButton.activityIndicator.startAnimating()
            } else {
                self?.registerButton.activityIndicator.stopAnimating()
            }
        }
        
        viewModel.registerAction.values.observeValues { (user) in
            NetworkScenarios.showAuthorizedScreen(animated: true)
        }
    }
    
    deinit {
        print("SIGNUP DEINIT")
    }

    @IBAction func buttonAcceptTermsTouched(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        viewModel.termsAccepted.value = sender.isSelected
    }

}

extension SignUpViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        (textField as! SignUpTextField).set(state: .Active)
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        (textField as! SignUpTextField).set(state: .Normal)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let index = textFieldsCollection.index(of: textField as! SignUpTextField)
        
        guard index != textFieldsCollection.count - 1 else { return true }
        
        textFieldsCollection[index! + 1].becomeFirstResponder()
        
        return false
    }
}
