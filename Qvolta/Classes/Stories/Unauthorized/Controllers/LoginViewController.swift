//
//  LoginViewController.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 1/28/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import UIKit

import ReactiveSwift
import ReactiveCocoa

class LoginViewController: UIViewController {
    @IBOutlet weak var signUpLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var loginTextField: SignUpTextField!
    @IBOutlet weak var passwordTextField: SignUpTextField!
    
    @IBOutlet weak var loginButton: GradientButton!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    
    @IBOutlet weak var signUpButton: UIButton!
    
    
    fileprivate var viewModel: LoginViewModel = LoginViewModel()
    
    var alert: TopAlertView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = nil
        theme()
        setupReactive()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func setupReactive() {
        viewModel.login <~ loginTextField.reactive.continuousTextValues
        viewModel.password <~ passwordTextField.reactive.continuousTextValues
        
        loginButton.reactive.pressed = CocoaAction(viewModel.loginAction)
        
        viewModel.loginAction.isExecuting.signal.observeValues { [weak self] (isExecuting) in
            self?.view.endEditing(true)
            
            if isExecuting == true {
                self?.alert?.hide()
                self?.loginButton.activityIndicator.startAnimating()
            } else {
                self?.loginButton.activityIndicator.stopAnimating()
            }
        }
        
        viewModel.loginAction.events.observeValues { [weak self] (event) in
            self?.alert = nil
            
            switch event {
            case .completed:
                NetworkScenarios.showAuthorizedScreen(animated: true)
                print("Completed!")
            case .failed(let error):
                self?.loginTextField.set(state: .Error)
                self?.passwordTextField.set(state: .Error)
                
                self?.alert = TopAlertView.show(title: "Password or email is incorrent.\nPlease try again",
                                                  backgroundColor: .paleRed)
                print(error)
            default:
                print("Default")
            }
        }
    }
    
    deinit {
        print("DEINIT LOGIN")
    }
    
    func theme() {
        view.backgroundColor = .gray4
        
        titleLabel.textColor = .gray2
        signUpLabel.textColor = .gray2
        
        loginButton.layer.cornerRadius = 8.0
        loginButton.setupWithColors(left: .blueBlue, right: .turquoise, shadow: .turquoise)
        
        forgotPasswordButton.tintColor = .niceBlue
        signUpButton.tintColor = .niceBlue
        
        headerView.layer.shadowColor = UIColor.lightBlueGrey.cgColor
        headerView.layer.shadowOpacity = 0.7
        headerView.layer.shadowRadius = 20
        headerView.layer.shadowOffset = CGSize(width: 0, height: 6)
    }
    
    // MARK: - IBActions
    
    @IBAction func buttonForgotTouched(_ sender: Any) {
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        (textField as! SignUpTextField).set(state: .Active)
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        (textField as! SignUpTextField).set(state: .Normal)
    }
}
