//
//  ForgotPasswordViewModel.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/2/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import Foundation
import ReactiveSwift
import ReactiveCocoa

class ForgotPasswordViewModel {
    
    struct FormError: Error {
        let reason: String
        
        static let invalid = FormError(reason: "Form is invalid.")
    }
    
    var email: ValidatingProperty<String?, FormError> = ValidatingProperty(nil) { input in
        guard let email = input, email.count > 0, email.isValidEmail else { return .invalid(.invalid) }
        
        return .valid
    }
}
