//
//  BuySellViewModel.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/7/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import Foundation
import ReactiveCocoa
import ReactiveSwift
import Result

class BuySellViewModel {
    var coins: [Coin] = [.BTC, .ETH]
    
    var countries: [Country] = Session.shared.configuration.countries
    
    var payments: [PaymentSystem] = Session.shared.configuration.paymentMethods
    
    var activity: MutableProperty<BuySellSearch.Activity?> = MutableProperty(nil)
    var coin: MutableProperty<Coin?> = MutableProperty(nil)
    var amount: MutableProperty<Double> = MutableProperty(0)
    var country: MutableProperty<Country?> = MutableProperty(nil)
    var payment: MutableProperty<PaymentSystem?> = MutableProperty(nil)
    
    var activityAction: Action<BuySellSearch.Activity, (), NoError>!
    var coinAction: Action<Coin, (), NoError>!
    
    var searchAction: Action<(), BuySellSearch, APIError>!
    
    var searchRequest: BuySellSearch?
    
    init() {
        self.activityAction = Action {
            [weak self] activity in SignalProducer { observer, _ in
                self?.activity.value = activity
                observer.sendCompleted()
            }
        }
        
        self.coinAction = Action {
            [weak self] coin in SignalProducer { observer, _ in
                self?.coin.value = coin
                observer.sendCompleted()
            }
        }

        let isFormReady: Property<Bool> =
            Property.combineLatest(activity, coin, country, payment)
                .map { activity, coin, country, payment -> Bool in
                    return activity != nil &&
                        coin != nil &&
                        country != nil &&
                        payment != nil
            }
        
        searchAction = Action(enabledIf: isFormReady, execute: {
            SignalProducer<BuySellSearch, APIError> { [weak self] observer, _ in
                self?.searchRequest = BuySellSearch()
                self?.searchRequest?.activity = self?.activity.value
                self?.searchRequest?.coin = self?.coin.value
                self?.searchRequest?.amount = Double((self?.amount.value)!)
                self?.searchRequest?.payment = self?.payment.value
                self?.searchRequest?.country = self?.country.value
            
                observer.send(value: (self?.searchRequest)!)
                observer.sendCompleted()
            }
        })
    }
    
    func actualize() -> SignalProducer<(), AnyError> {
        return SignalProducer { [weak self] observer, _ in
            guard let request = self?.searchRequest else {
                observer.send(error: AnyError(NSError.init()))
                
                return
            }
            
            self?.activity.value = request.activity
            self?.coin.value = request.coin
            self?.country.value = request.country
            self?.amount.value = request.amount
            self?.payment.value = request.payment
            
            observer.sendCompleted()
        }
    }
}
