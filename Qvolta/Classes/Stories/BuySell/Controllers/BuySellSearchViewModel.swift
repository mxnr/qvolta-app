//
//  BuySellSearchViewModel.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/26/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import Foundation
import ReactiveSwift
import ReactiveCocoa

class BuySellSearchViewModel {
    var searchRequest: MutableProperty<BuySellSearch>!
    var currentProperty: MutableProperty<BuySellSearch.Property> = MutableProperty(.Activity)
    
    var pickerArray: MutableProperty<[String]> = MutableProperty([])
    
    var activities: [BuySellSearch.Activity] = [.Buy, .Sell]
    var coins: [Coin] = [.BTC, .ETH]
    var countries: [Country] = Session.shared.configuration.countries
    
    var payments: [PaymentSystem] = Session.shared.configuration.paymentMethods
    
    
    var ordersArray: MutableProperty<[TradeOrder]> = MutableProperty([])
 
    init(with request: BuySellSearch) {
        searchRequest = MutableProperty(request)
        
        setupPickerArray()
        currentProperty.signal.observeValues { [weak self] (property) in
            self?.setupPickerArray()
        }
        
        setupStub()
    }
    
    func setupStub() {
        var user = User(id: 2, username: "Phillip Sanders")
        user.isVerified = true
        let payment = Session.shared.configuration.paymentMethods.first!
        let order = TradeOrder(user: user,
                               paymentSystem: payment,
                               priceCoin: 4560000,
                               priceCurrency: 14560000,
                               orderType: TradeOrder.OrderType.Buy,
                               coin: .BTC)
        let order2 = TradeOrder(user: User(id: 3, username: "Dustin Blair"),
                                paymentSystem: payment,
                                priceCoin: 4560000,
                                priceCurrency: 14560000,
                                orderType: TradeOrder.OrderType.Buy,
                                coin: .ETH)
        
        ordersArray.value = [order, order2, order, order, order]
    }
    
    func setupPickerArray() {
        switch currentProperty.value {
        case .Activity:
            pickerArray.value = activities.map({ $0.rawValue })
            break
        case .Amount:
            break
        case .Coin:
            pickerArray.value = coins.map({ $0.rawValue })
            break
        case .Country:
            pickerArray.value = countries.map({ $0.name })
            break
        case .Payment:
            pickerArray.value = payments.map({ $0.name })
            break
        }
    }
    
    func didEnterUniqueAmount(_ amount: String) -> Bool {
        let newAmount: Double = (amount as NSString).doubleValue
        
        if newAmount == searchRequest.value.amount {
            return false
        }
        
        searchRequest.value.amount = newAmount
        
        return true
    }
    
    func didSelectUniqueValueAtRow(_ row: Int) -> Bool{
        switch currentProperty.value {
        case .Activity:
            if activities[row] == searchRequest.value.activity {
                return false
            }
            searchRequest.value.activity = activities[row]
            break
        case .Amount:
            break
        case .Coin:
            if coins[row].settings.name == searchRequest.value.coin.settings.name {
                return false
            }
            
            searchRequest.value.coin = coins[row]
            break
        case .Country:
            if countries[row].name == searchRequest.value.country.name {
                return false
            }
            searchRequest.value.country = countries[row]
            break
        case .Payment:
            if payments[row].name == searchRequest.value.payment.name {
                return false
            }
            
            searchRequest.value.payment = payments[row]
            break
        }
        
        return true
    }
}
