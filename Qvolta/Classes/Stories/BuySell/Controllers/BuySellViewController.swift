//
//  BuySellViewController.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/2/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import UIKit
import SnapKit

import ReactiveCocoa
import ReactiveSwift
import Result

class BuySellViewController: UIViewController {
    
    // MARK: - Outlets and vars
    
    enum Step {
        case Activity
        case Coin
        case Search
    }
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var firstView: UIView!
    
    @IBOutlet weak var firstViewTitleLabel: UILabel!
    @IBOutlet weak var sellButton: GradientButton!
    @IBOutlet weak var buyButton: GradientButton!
    
    @IBOutlet weak var coinViewTitleLabel: UILabel!
    
    @IBOutlet weak var selectCoinStackView: UIStackView!
    
    @IBOutlet var swipeDownRecognizer: UISwipeGestureRecognizer!
    
    @IBOutlet weak var searchTitleLabel: UILabel!
    
    @IBOutlet weak var searchAmountCoinLabel: UILabel!
    @IBOutlet weak var searchAmountCoinTextField: UITextField!
    @IBOutlet weak var searchAmountCoinImageView: UIImageView!

    @IBOutlet weak var searchAmountLabel: UILabel!
    @IBOutlet weak var searchAmountTextField: UITextField!
    
    @IBOutlet weak var searchSpecifyCenterLabel: UILabel!
    
    @IBOutlet weak var searchCountryLabel: UILabel!
    @IBOutlet weak var searchCountryTextField: UITextField!

    @IBOutlet weak var searchPaymentLabel: UILabel!
    @IBOutlet weak var searchPaymentTextField: UITextField!

    @IBOutlet weak var searchCurrencyTextField: UITextField!

    @IBOutlet weak var searchButton: GradientButton!
    
    @IBOutlet var textFieldsArray: [UITextField]!
    
    @IBOutlet var textFiedsWithPickerArray: [UITextField]!
    
    fileprivate weak var currentTextField: UITextField?
    
    
//    var coin: Coin!
//    var activity: BuySellSearch.Activity!
    var currentStep: Step = .Activity
    
    let viewModel: BuySellViewModel = BuySellViewModel()
    
    let pickerView: UIPickerView = UIPickerView()
}

extension BuySellViewController {
    
    // MARK: - View setup
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.gray4
        
        pickerView.dataSource = self
        pickerView.delegate = self
        
        setupSelectBuySellView()
        setupSelectCoinView()
        setupSearchView()
        
        setupReactive()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default
            .addObserver(self,
                         selector: #selector(keyboardWillShow),
                         name: NSNotification.Name.UIKeyboardWillShow,
                         object: nil)
        
        NotificationCenter.default
            .addObserver(self,
                         selector: #selector(keyboardWillHide),
                         name: NSNotification.Name.UIKeyboardWillHide,
                         object: nil)
        
        viewModel.actualize().startWithResult { [weak self] (result) in
            switch result {
            case .success():
                self?.searchAmountCoinTextField.text = "\(self?.viewModel.amount.value.cleanValue ?? "0")"
                self?.searchCountryTextField.text = self?.viewModel.country.value?.name
                self?.searchPaymentTextField.text = self?.viewModel.payment.value?.name
                
                self?.moveToStep(.Search)
                break;
            case .failure(_):
                break
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }
    
    func setupSelectBuySellView() {
        firstView.snp.makeConstraints { (make) in
            make.height.equalTo(view.snp.height)
        }
        
        view.layoutIfNeeded()
        
        sellButton.setupWithColors(left: .blueBlue, right: .turquoise, shadow: .turquoise)
        buyButton.setupWithColors(left: .darkblue, right: .blueBlueTwo, shadow: .coolBlue60)
        
        sellButton.reactive.pressed = CocoaAction(viewModel.activityAction,
                                                  { [weak self] (_) -> BuySellSearch.Activity in
                                                    self?.moveToStep(.Coin)
                                                    return .Sell
        })
        
        buyButton.reactive.pressed = CocoaAction(viewModel.activityAction,
                                                  { [weak self] (_) -> BuySellSearch.Activity in
                                                    self?.moveToStep(.Coin)
                                                    return .Buy
        })
    }
    
    func setActivity(_ activity: BuySellSearch.Activity) {
        coinViewTitleLabel.text = "And what do you what to \(activity.rawValue.lowercased())?"
    }
    
    func setupReactive() {
        viewModel.activity.producer.startWithValues { [weak self] (activity) in
            guard let activity = activity else { return }
            
            self?.setActivity(activity)
        }
        
        viewModel.coin.producer.startWithValues { [weak self] (coin) in
            guard let coin = coin else { return }
            
            self?.setCoin(coin)
        }
        
        viewModel.amount <~ searchAmountCoinTextField.reactive.continuousTextValues.map({ (amount) -> Double in
            guard let amount = amount else { return 0 }
            
            return (amount as NSString).doubleValue
        })
        
        viewModel.country.signal.observeValues { [weak self] (country) in
            self?.searchCountryTextField.text = country?.name
        }
        
        viewModel.payment.signal.observeValues { [weak self] (payment) in
            self?.searchPaymentTextField.text = payment?.name
        }

        viewModel.searchAction.values.observeValues({ [weak self] (request) in
            self?.showSearchResultController(request)
        })

        searchButton.reactive.pressed = CocoaAction(viewModel.searchAction)
    }
    
    func showSearchResultController(_ model: BuySellSearch) {
        let controller: BuySellSearchViewController = storyboard?.instantiateViewController(withIdentifier: "BuySellSearchViewController") as! BuySellSearchViewController
        
        controller.viewModel = BuySellSearchViewModel(with: model)
        
        present(controller, animated: true, completion: nil)
    }
    
    func setupSelectCoinView() {
        viewModel.coins.forEach { (coin) in
            let button: GradientButton = GradientButton()
            selectCoinStackView.addArrangedSubview(button)
            button.snp.makeConstraints({ (make) in
                make.height.equalTo(60)
            })
            
            let settings = coin.settings
            
            button.setTitle(settings.name, for: .normal)
            
            button.addIcon(settings.logoBig)
            button.setupWithColors(left: settings.firstColor,
                                   right: settings.secondColor,
                                   shadow: settings.shadowColor)
            
            button.reactive.pressed = CocoaAction(viewModel.coinAction,
                                                      { [weak self] (_) -> Coin in
                                                        self?.moveToStep(.Search)
                                                        return coin
            })
        }
    }
    
    func setCoin(_ coin: Coin) {
        let settings = coin.settings
        
        searchTitleLabel.text = "How much \(settings.name) do you want to \(viewModel.activity.value!.rawValue.lowercased())?"
        
        searchButton.setupWithColors(left: settings.firstColor,
                                     right: settings.secondColor,
                                     shadow: settings.shadowColor)
        searchAmountCoinImageView.image = settings.logoSmall
        searchAmountCoinLabel.text = "Amount, \(settings.code)"
        
        let toolbar = UIToolbar.withButton(tintColor: settings.firstColor,
                                           target: self,
                                           selector: #selector(textFieldNext))
        textFieldsArray.forEach { (textField) in
            if textField == self.searchAmountTextField {
                return
            }
            textField.inputAccessoryView = toolbar
        }
    }
    
    func setupSearchView() {
        searchTitleLabel.font = UIFont.systemFont(ofSize: 28, weight: .bold)
        searchTitleLabel.textColor = .gray1
        
        searchSpecifyCenterLabel.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        searchSpecifyCenterLabel.textColor = .gray3
        
        let titles: [UILabel] = [searchAmountCoinLabel, searchAmountLabel, searchCountryLabel, searchPaymentLabel]
        
        titles.forEach { (label) in
            label.textColor = .gray2
            label.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        }
        
        textFieldsArray.forEach { (textField) in
            textField.delegate = self
            
            textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 1))
            textField.leftViewMode = .always
            textField.font = UIFont.systemFont(ofSize: 14, weight: .regular)
            textField.textColor = .gray1
            
            textField.layer.borderColor = UIColor.gray3.cgColor
            textField.layer.borderWidth = 1.0
            textField.layer.cornerRadius = 6.0
            
            textField.backgroundColor = .white
        }
        
        searchCurrencyTextField.delegate = self
        searchCurrencyTextField.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        searchCurrencyTextField.backgroundColor = .white
        searchCurrencyTextField.textColor = .gray1
        
        textFiedsWithPickerArray.forEach { (textField) in
            textField.inputView = pickerView
        }
    }
    
    @objc func textFieldNext() {
        guard let currentTextField = currentTextField else { return }
        
        if currentTextField.text?.count == 0,
            currentTextField == searchPaymentTextField || currentTextField == searchCountryTextField {
            pickerView(pickerView, didSelectRow: 0, inComponent: 0)
        }
        
        guard textFieldsArray.last! != currentTextField else {
            view.endEditing(false)
            return
        }
        
        let index = textFieldsArray.index(of: currentTextField)! + 1
        let nextTextField = textFieldsArray[index]
        
        if nextTextField != searchAmountTextField {
            nextTextField.becomeFirstResponder()
        } else {
            textFieldsArray[index + 1].becomeFirstResponder()
        }
    }
    
    func moveToStep(_ step: Step) {
        var y: CGFloat = 0
        
        switch step {
        case .Activity:
            y = 0.0
            break;
        case .Coin:
            y = view.frame.size.height
            break;
        case .Search:
            y = view.frame.size.height * 2
            break;
        }

        currentStep = step
        
        scrollView.scrollRectToVisible(CGRect.init(x: 0, y: y, width: 1, height: view.frame.size.height), animated: true)
    }

    @IBAction func didVerticalSwipe(_ sender: UISwipeGestureRecognizer) {
        switch currentStep {
        case .Activity:
            return
        case .Coin:
            moveToStep(.Activity)
            break
        case .Search:
            moveToStep(.Coin)
            break
        }
    }
    
    @IBAction func didBackgroundTap() {
        view.endEditing(false)
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        let userInfo = notification.userInfo as! [String: NSObject] as NSDictionary
        let keyboardFrame = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as! CGRect
        let keyboardHeight = keyboardFrame.height
        
        scrollView.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight, 0)
        scrollView.scrollRectToVisible(scrollView.convert(currentTextField!.frame, from: currentTextField!.superview), animated: true)
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        scrollView.contentInset = UIEdgeInsets.zero
    }
}

extension BuySellViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayForPicker().count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrayForPicker()[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if currentTextField == searchCountryTextField {
            viewModel.country.value = viewModel.countries[row]
        } else if currentTextField == searchPaymentTextField {
            viewModel.payment.value = viewModel.payments[row]
        }
    }
    
    func arrayForPicker() -> [String] {
        var array: [String]!
        
        if currentTextField == searchCountryTextField {
            array = viewModel.countries.map({ (country) -> String in
                country.name
            })
        } else if currentTextField == searchPaymentTextField {
            array = viewModel.payments.map({ (payment) -> String in
                payment.name
            })
        } else {
            array = []
        }
        
        return array
    }
}

extension BuySellViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        currentTextField = textField
        scrollView.scrollRectToVisible(scrollView.convert(textField.frame, from: textField.superview), animated: true)
        pickerView.reloadAllComponents()
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let _ = textFiedsWithPickerArray.index(of: textField) {
            return false
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
}
