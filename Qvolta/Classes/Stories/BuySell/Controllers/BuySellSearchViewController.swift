//
//  BuySellSearchViewController.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/26/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import UIKit

class BuySellSearchViewController: UIViewController {
    
    var pickerView: UIPickerView! = UIPickerView()
    var shouldShowAmount: Bool = false
    
    @IBOutlet weak var closeButton: UIButton!
    
    @IBOutlet weak var searchTitleLabel: UILabel!
    @IBOutlet weak var searchHorizontalStackView: UIStackView!
    @IBOutlet weak var searchFirstVerticalStackView: UIStackView!

    @IBOutlet weak var tableViewTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: BuySellSearchViewModel!
    
    var hiddenTextField: UITextField = UITextField()
    
    @IBOutlet weak var darkBackgroundView: UIView!
    
    @IBOutlet weak var amountContainerView: UIView!
    @IBOutlet weak var amountTextField: BorderedTextField!
    @IBOutlet weak var amountTitleLabel: UILabel!
    
    @IBOutlet weak var amountContainerBottomConstraint: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib.init(nibName: "TradeTableViewCell", bundle: nil), forCellReuseIdentifier: "TradeTableViewCell")
        pickerView.delegate = self
        pickerView.dataSource = self
        refreshSearchConditions()
        theme()
        setupReactive()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default
            .addObserver(self,
                         selector: #selector(keyboardWillShow),
                         name: NSNotification.Name.UIKeyboardWillShow,
                         object: nil)
        
        NotificationCenter.default
            .addObserver(self,
                         selector: #selector(keyboardWillHide),
                         name: NSNotification.Name.UIKeyboardWillHide,
                         object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }
    
    func theme() {
        searchTitleLabel.textColor = .gray3
        amountTitleLabel.textColor = .gray2
        view.backgroundColor = .gray4
        tableView.separatorColor = .gray4
        tableView.backgroundColor = .gray4
        
        let inputAccessoryView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 50))
        inputAccessoryView.backgroundColor = .gray4
        let updateButton: GradientButton = GradientButton(type: .custom)
        updateButton.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: 50)
        updateButton.setupWithColors(left: .blueBlue, right: .turquoise, shadow: nil)
        updateButton.cornerRadius = 0
        updateButton.setTitle("Update", for: .normal)
        updateButton.addTarget(self, action: #selector(buttonDoneTouched), for: .touchUpInside)
        inputAccessoryView.addSubview(updateButton)
        
        hiddenTextField.inputAccessoryView = inputAccessoryView
        hiddenTextField.inputView = pickerView
        view.addSubview(hiddenTextField)
        hiddenTextField.alpha = 0.0
        
        amountTextField.inputAccessoryView = inputAccessoryView
        
        amountContainerBottomConstraint.constant = -amountContainerView.frame.size.height
    }
    
    func setupReactive() {
        closeButton.reactive.controlEvents(.touchUpInside).observeValues { [weak self] (_) in
            self?.dismiss(animated: true, completion: nil)
        }
        
        viewModel.ordersArray.producer.startWithValues { [weak self] (orders) in
            self?.tableViewTitle.text = String(format: NSLocalizedString("ActiveTrades", comment: ""), orders.count)
            self?.tableView.reloadData()
        }
        
        viewModel.pickerArray.producer.startWithValues { [weak self] (_) in
            self?.pickerView.reloadComponent(0)
        }
    }
    
    func refreshSearchConditions() {
        let subviews = searchHorizontalStackView.arrangedSubviews
        subviews.forEach { (view) in
            if searchFirstVerticalStackView != view {
                view.removeFromSuperview()
            }
        }
        
        let verticalSubviews = searchFirstVerticalStackView.arrangedSubviews
        verticalSubviews.forEach { (view) in
            view.removeFromSuperview()
        }
        
        addTitle("I want to")
        
        addButton(color: .brandActive,
                  title: viewModel.searchRequest.value.activity.rawValue,
                  tag: BuySellSearch.Property.Activity.rawValue)
        
        addButton(color: .brandActive,
                  title: viewModel.searchRequest.value.amount.cleanValue,
                  tag: BuySellSearch.Property.Amount.rawValue)
        
        addButton(color: viewModel.searchRequest.value.coin.settings.firstColor,
                  title: viewModel.searchRequest.value.coin.settings.code,
                  tag: BuySellSearch.Property.Coin.rawValue)
        
        addTitle("in")
        
        addButton(color: .brandActive,
                  title: viewModel.searchRequest.value.country.name,
                  tag: BuySellSearch.Property.Country.rawValue)
        
        addTitle("with")
        
        addButton(color: .brandActive,
                  title: viewModel.searchRequest.value.payment.name,
                  tag: BuySellSearch.Property.Payment.rawValue)
        
        view.layoutIfNeeded()
    }
    
    func addButton(color: UIColor, title: String, tag: Int) {
        let button: BorderButton = BorderButton(type: .custom)
        button.setup(color: color,
                     title: title,
                     font: UIFont.systemFont(ofSize: 14, weight: .regular))
        button.tag = tag
        button.addTarget(self, action: #selector(didButtonTouch), for: .touchUpInside)
        
        addViewToStacksView(view: button)
    }
    
    func addTitle(_ string: String) {
        let title: UILabel = UILabel()
        title.font = UIFont.systemFont(ofSize: 23, weight: .semibold)
        title.text = string
        
        addViewToStacksView(view: title)
    }
    
    @objc func didButtonTouch(sender: UIButton) {
        viewModel.currentProperty.value = BuySellSearch.Property(rawValue: sender.tag)!
        
        if sender.tag == BuySellSearch.Property.Amount.rawValue {
            shouldShowAmount = true
            amountTextField.becomeFirstResponder()
        } else {
            shouldShowAmount = false
            hiddenTextField.becomeFirstResponder()
        }
    }
    
    func addViewToStacksView(view: UIView) {
        view.sizeToFit()
        
        if view is UIButton {
            view.frame = CGRect.init(x: 0, y: 0, width: view.frame.insetBy(dx: -12, dy: 0).width, height: 30)
        }
        
        let lastStackView = searchHorizontalStackView.arrangedSubviews.last as! UIStackView
        
        let widths: CGFloat = lastStackView.arrangedSubviews.reduce(into: CGFloat()) { (width, view) in
            width += view.frame.size.width + searchHorizontalStackView.spacing
        }
        
        if widths + view.frame.size.width + lastStackView.spacing > searchHorizontalStackView.frame.size.width {
            let newStackView = UIStackView(arrangedSubviews: [view])
            newStackView.alignment = .leading
            newStackView.spacing = lastStackView.spacing
            newStackView.distribution = .equalSpacing
            newStackView.axis = .horizontal
            searchHorizontalStackView.addArrangedSubview(newStackView)
        } else {
            lastStackView.addArrangedSubview(view)
        }
        
        view.snp.makeConstraints { (make) in
            make.width.equalTo(view.frame.width)
            make.height.equalTo(30)
        }
    }

    @IBAction func buttonDoneTouched(_ sender: Any) {
        if shouldShowAmount, viewModel.didEnterUniqueAmount(amountTextField.text ?? "0.0") {
            refreshSearchConditions()
        }
        
        if !shouldShowAmount,  viewModel.didSelectUniqueValueAtRow(pickerView.selectedRow(inComponent: 0)) {
            refreshSearchConditions()
        }
        
        view.endEditing(true)
    }
    
    @IBAction func didBackgroundTap() {
        view.endEditing(false)
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        let userInfo = notification.userInfo as! [String: NSObject] as NSDictionary
        let keyboardFrame = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as! CGRect
        let keyboardHeight = keyboardFrame.height
        let duration = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        
        if self.shouldShowAmount {
            self.amountContainerBottomConstraint.constant = keyboardHeight
            self.amountTextField.text = viewModel.searchRequest.value.amount.cleanValue
            self.amountTextField.image = viewModel.searchRequest.value.coin.settings.logoSmall
        }

        UIView.animate(withDuration: duration) {
            self.view.layoutIfNeeded()
            self.darkBackgroundView.alpha = 1.0
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        let userInfo = notification.userInfo as! [String: NSObject] as NSDictionary
        let duration = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        
        UIView.animate(withDuration: duration) {
            if self.shouldShowAmount {
                self.amountContainerBottomConstraint.constant = -self.amountContainerView.frame.size.height
            }
            
            self.darkBackgroundView.alpha = 0.0
            self.shouldShowAmount = false
        }
    }
}

extension BuySellSearchViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return viewModel.pickerArray.value.count
    }
}

extension BuySellSearchViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return viewModel.pickerArray.value[row]
    }
}

extension BuySellSearchViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.ordersArray.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TradeTableViewCell") as! TradeTableViewCell
        
        let order = viewModel.ordersArray.value[indexPath.row]
        cell.bind(order)
        
        return cell
    }
}

