//
//  WalletCoinItemView.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/4/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import UIKit

class WalletCoinItemView: UIControl {

    @IBOutlet weak var chevronImageView: UIImageView!
    @IBOutlet weak var logoImageView: UIImageView!
    
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var balanceTitleLabel: UILabel!
    
    @IBOutlet weak var coinNameLabel: UILabel!
    
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var receiveButton: UIButton!
    
    var coin: Coin!
    
    var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    static let height: Float = 175.0
    
    override public class var layerClass: Swift.AnyClass {
        return CAGradientLayer.self
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        sendButton.layer.borderColor = UIColor.white.cgColor
        sendButton.layer.borderWidth = 1.0
        sendButton.layer.cornerRadius = 8.0
        receiveButton.layer.borderColor = UIColor.white.cgColor
        receiveButton.layer.borderWidth = 1.0
        receiveButton.layer.cornerRadius = 8.0
        
        balanceLabel.textColor = .white
        coinNameLabel.textColor = UIColor(white: 1.0, alpha: 0.7)
        balanceTitleLabel.textColor = UIColor(white: 1.0, alpha: 0.7)
        
        let gradientLayer = self.layer as! CAGradientLayer
        gradientLayer.startPoint = CGPoint(x: 0, y: 1)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
    }
    
    var needsToBeAnimated: Bool = false
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.shadowPath = CGPath(rect: bounds.insetBy(dx: 10, dy: 0), transform: nil)
    }
    
    func setup(with coin: Coin) {
        self.coin = coin
        
        let settings = coin.settings
        
        coinNameLabel.text = settings.code
        logoImageView.image = settings.logoBig
        
        sendButton.setTitle("Send \(settings.code)", for: .normal)
        receiveButton.setTitle("Receive \(settings.code)", for: .normal)
        
        setupWithColors(left: settings.firstColor,
                        right: settings.secondColor,
                        shadow: settings.shadowColor)
    }
    
    func setupWithColors(left: UIColor, right: UIColor, shadow: UIColor) {
        
        let gradientLayer = self.layer as! CAGradientLayer
        gradientLayer.colors = [left.cgColor, right.cgColor]
        gradientLayer.locations = [0.0, 1.0]
        
        layer.shadowColor = shadow.cgColor
        layer.shadowOpacity = 0.6
        layer.shadowOffset = CGSize(width: 0, height: 10)
        layer.shadowRadius = 20
    }
}
