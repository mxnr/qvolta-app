//
//  CircleWalletDetailsView.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/7/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import UIKit
import SnapKit

@IBDesignable class CircleWalletDetailsView: UIView {
    
    private var circleLayer: CAShapeLayer!
    private var gradientLayer: CAGradientLayer = CAGradientLayer()

    var titleLabel: UILabel = UILabel()
    var countLabel: UILabel = UILabel()
    private var centerContainerView: UIView = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        
        setup()
    }
    
    func setup() {
        backgroundColor = .clear
        clipsToBounds = false
        
        titleLabel.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        titleLabel.textColor = .gray2
        titleLabel.textAlignment = .center
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.minimumScaleFactor = 0.5
        countLabel.font = UIFont.systemFont(ofSize: 45, weight: .regular)
        countLabel.textColor = .gray1
        countLabel.textAlignment = .center
        
        addSubview(centerContainerView)
        centerContainerView.addSubview(titleLabel)
        centerContainerView.addSubview(countLabel)
        
        centerContainerView.snp.makeConstraints {
            $0.left.right.equalToSuperview()
            $0.centerY.equalToSuperview().offset(3)
            
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.top.left.right.equalToSuperview().inset(UIEdgeInsetsMake(0, 15, 0, 15))
        }
        
        countLabel.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
            make.top.equalTo(titleLabel.snp.bottom)
        }
        
        setup(colors: .richBlue,
              second: .brightSkyBlue,
              title: "Turnover, $",
              count: "100")
        
        layoutIfNeeded()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        setupSize()
    }
    
    func setupSize() {
        gradientLayer.frame = bounds
        
        let path: UIBezierPath = UIBezierPath(arcCenter: CGPoint(x: bounds.midX,
                                                                 y: bounds.midY),
                                              radius: bounds.width / 2 - 3,
                                              startAngle: CGFloat(-Double.pi / 2),
                                              endAngle: CGFloat(Double.pi + Double.pi / 2),
                                              clockwise: true)
        
        circleLayer.path = path.cgPath
        
        gradientLayer.mask = circleLayer
    }
    
    func setup(colors first: UIColor, second: UIColor, title: String, count: String) {
        gradientLayer.colors = [first.cgColor, second.cgColor]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.startPoint = CGPoint(x: 0, y: 1)
        gradientLayer.endPoint = CGPoint(x: 0, y: 0)
        gradientLayer.frame = bounds
        layer.addSublayer(gradientLayer)
        
        circleLayer = CAShapeLayer()
        circleLayer.fillColor = UIColor.clear.cgColor
        circleLayer.strokeColor =  UIColor(red: 16.0 / 255.0, green: 217.0 / 255.0, blue: 68.0 / 255.0, alpha: 1.0).cgColor
        circleLayer.lineWidth = 5
        circleLayer.lineCap = kCALineCapRound
        
        titleLabel.text = title
        countLabel.text = count
    }

}
