//
//  WalletModalDraggableAnimator.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/14/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import UIKit

class WalletModalDraggableAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    var isPresenting: Bool = true
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return (isPresenting) ? 0.4 : 0.2
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        if isPresenting {
            animatePresentTransition(using: transitionContext)
        } else {
            animateDissmissTransition(using: transitionContext)
        }
    }
    
    func animatePresentTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        
        let toController: WalletModalDraggableViewController = transitionContext.viewController(forKey: .to) as! WalletModalDraggableViewController
        
        containerView.addSubview(toController.view)
        toController.view.backgroundColor = .clear
        
        toController.contentView.transform = CGAffineTransform(translationX: 0, y: toController.contentView.frame.size.height + 100)
        
        UIView.animate(withDuration: 0.4,
                       delay: 0,
                       usingSpringWithDamping: 0.8,
                       initialSpringVelocity: 0.5,
                       options: .curveEaseOut,
                       animations: {
                        toController.view.backgroundColor = UIColor(white: 0.0, alpha: 0.3)
                        toController.contentView.transform = CGAffineTransform.identity
        }) { (completed) in
            transitionContext.completeTransition(true)
        }
    }
    
    func animateDissmissTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let fromController: WalletModalDraggableViewController = transitionContext.viewController(forKey: .from) as! WalletModalDraggableViewController
        
        UIView.animate(withDuration: 0.2,
                       animations: {
                        fromController.view.alpha = 0.0
        }) { (completed) in
            transitionContext.completeTransition(true)
        }
    }
}
