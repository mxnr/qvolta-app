//
//  WalletSellViewController.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/15/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import UIKit

class WalletSellViewController: WalletModalDraggableViewController {
    
    @IBOutlet weak var mainTitleLabel: UILabel!
    
    @IBOutlet weak var textFieldsScrollView: UIScrollView!
    
    @IBOutlet var titleLabelsArray: [UILabel]!
    @IBOutlet var textFieldsArray: [UITextField]!
    
    @IBOutlet weak var coinIconImageView: UIImageView!
    
    @IBOutlet weak var sendButton: GradientButton!
    
    var isKeyboardShown: Bool = false
    
    var coin: Coin!
    
    weak var currentTextField: UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        theme()
        setupTextFields()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default
            .addObserver(self,
                         selector: #selector(keyboardWillShow),
                         name: NSNotification.Name.UIKeyboardWillShow,
                         object: nil)
        
        NotificationCenter.default
            .addObserver(self,
                         selector: #selector(keyboardWillHide),
                         name: NSNotification.Name.UIKeyboardWillHide,
                         object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }
    
    func setupTextFields() {
        let toolbar = UIToolbar.withButton(tintColor: coin.settings.firstColor,
                                               target: self,
                                               selector: #selector(textFieldNext))
        
        titleLabelsArray.forEach { (label) in
            label.font = UIFont.systemFont(ofSize: 14, weight: .regular)
            label.textColor = .gray2
        }
        
        textFieldsArray.forEach { (textField) in
            textField.inputAccessoryView = toolbar
            textField.layer.borderColor = UIColor.gray3.cgColor
            textField.layer.borderWidth = 1.0
            textField.layer.cornerRadius = 6.0
            
            textField.backgroundColor = .white
            
            textField.delegate = self
            
            textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 1))
            textField.leftViewMode = .always
            textField.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        }
    }
    
    func theme() {
        mainTitleLabel.font = UIFont.systemFont(ofSize: 16, weight: .heavy)
        mainTitleLabel.textColor = .gray1
        
        let settings = coin.settings
        
        coinIconImageView.image = settings.logoSmall
        
        sendButton.setupWithColors(left: settings.firstColor,
                                   right: settings.secondColor,
                                   shadow: settings.shadowColor)
        sendButton.setTitle("Send \(settings.name)", for: .normal)
    }
}

extension WalletSellViewController {
    
    @objc func keyboardWillShow(notification: Notification) {
        isKeyboardShown = true
        textFieldsScrollView.isScrollEnabled = true
        
        let userInfo = notification.userInfo as! [String: NSObject] as NSDictionary
        let keyboardFrame = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as! CGRect
        let keyboardHeight = keyboardFrame.height
        
        textFieldsScrollView.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight, 0)
        
        let animationDuration: TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        
        UIView.animate(withDuration: animationDuration) {
            self.view.transform = CGAffineTransform(translationX: 0, y: -self.contentView.frame.origin.y + UIApplication.shared.statusBarFrame.size.height)
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        isKeyboardShown = false
        textFieldsScrollView.isScrollEnabled = false
        textFieldsScrollView.contentInset = UIEdgeInsets.zero
        
        let userInfo = notification.userInfo as! [String: NSObject] as NSDictionary
        let animationDuration: TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        
        UIView.animate(withDuration: animationDuration) {
            self.view.transform = CGAffineTransform.identity
        }
    }
}

extension WalletSellViewController: UITextFieldDelegate {
    
    @objc func textFieldNext() {
        
        guard let currentTextField = currentTextField, textFieldsArray.last! != currentTextField else {
            view.endEditing(false)
            return
        }
        
        let index = textFieldsArray.index(of: currentTextField)!
        
        textFieldsArray[index + 1].becomeFirstResponder()
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        currentTextField = textField
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
}
