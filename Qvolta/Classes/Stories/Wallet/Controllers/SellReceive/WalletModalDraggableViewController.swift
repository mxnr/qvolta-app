//
//  WalletModalDraggableViewController.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/14/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import UIKit

class WalletModalDraggableViewController: UIViewController {

    @IBOutlet var panGestureRecognizer: UIPanGestureRecognizer!
    
    @IBOutlet weak var draggableIconView: UIView!
    
    @IBOutlet weak var contentView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        contentView.layer.cornerRadius = 8.0
        contentView.clipsToBounds = true
        
        draggableIconView.backgroundColor = .gray5
    }
    
    @IBAction func didTouchBackground(_ sender: Any) {
        self.dismiss(animated: true)
        
        UIView.animate(withDuration: 0.2,
                       animations: {
                        self.contentView.transform = CGAffineTransform(translationX: 0, y: self.contentView.bounds.size.height + 100)
        })
    }

    @IBAction func userDidPan(_ sender: UIPanGestureRecognizer) {
        view.endEditing(false)
        
        let translation = sender.translation(in: sender.view)
        let velocity = sender.velocity(in: sender.view)
        
        switch sender.state {
        case .changed:
            if translation.y < 0, contentView.transform.ty >= -10 {
                contentView.transform = CGAffineTransform(translationX: 0, y: contentView.transform.ty - 1)
            } else if translation.y > 0 {
                contentView.transform = CGAffineTransform(translationX: 0, y: translation.y)
            }
            
        case .cancelled:
            UIView.animate(withDuration: 0.2, animations: {
                self.contentView.transform = CGAffineTransform.identity
            })
        case .ended:
            if (contentView.transform.ty > contentView.bounds.size.height / 3) ||
                velocity.y > 1000 {
                UIView.animate(withDuration: 0.2,
                               animations: {
                                self.contentView.transform = CGAffineTransform(translationX: 0, y: self.contentView.bounds.size.height + 100)
                }, completion: { (completed) in
                    self.dismiss(animated: true)
                })
            } else {
                UIView.animate(withDuration: 0.4,
                               delay: 0,
                               usingSpringWithDamping: 0.6,
                               initialSpringVelocity: 0.2,
                               options: .curveEaseOut,
                               animations:  {
                    self.contentView.transform = CGAffineTransform.identity
                })
            }
        default:
            break;
        }
    }
}
