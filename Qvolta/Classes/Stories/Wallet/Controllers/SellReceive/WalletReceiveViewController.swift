//
//  WalletReceiveViewController.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/14/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import UIKit

class WalletReceiveViewController: WalletModalDraggableViewController {
    
    @IBOutlet weak var addressTitleLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var addressCopyButton: BorderButton!
    @IBOutlet weak var addressDetailsLabel: UILabel!
    
    @IBOutlet weak var separatorView: UIView!
    
    @IBOutlet weak var qrTitleLabel: UILabel!
    @IBOutlet weak var qrImageView: UIImageView!
    @IBOutlet weak var qrDetailsLabel: UILabel!

    var coin: Coin!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        theme()
    }
    
    func theme() {
        addressTitleLabel.textColor = .gray2
        addressLabel.textColor = .gray1
        
        let settings = coin.settings
        
        addressCopyButton.setup(color: settings.firstColor)
        
        addressDetailsLabel.textColor = .gray3
        addressDetailsLabel.text = "Use this \(settings.name) address to get \(settings.name)"
        
        separatorView.backgroundColor = .gray3
        
        qrTitleLabel.textColor = .gray2
        qrDetailsLabel.textColor = .gray3
    }
    
    @IBAction func buttonCopyTouched(_ sender: UIButton) {
        UIPasteboard.general.string = ""
        sender.isSelected = true
    }
}
