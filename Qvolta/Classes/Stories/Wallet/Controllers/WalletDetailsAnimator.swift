//
//  WalletDetailsAnimator.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/7/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import UIKit
import SnapKit

class WalletDetailsAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    var fromCoinView: WalletCoinItemView!
    
    var isPresenting: Bool = true
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        if isPresenting {
            animatePresentTransition(using: transitionContext)
        } else {
            animateDissmissTransition(using: transitionContext)
        }
    }
    
    func animatePresentTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        let containerView = transitionContext.containerView
        
        let fromController = transitionContext.viewController(forKey: .from)!
        
        let authorizedController: AuthorizedTabBarViewController = (fromController as! UINavigationController).topViewController! as! AuthorizedTabBarViewController
        
        let walletViewController: WalletViewController = authorizedController.currentViewController! as! WalletViewController
        let detailsViewController = transitionContext.viewController(forKey: .to) as! WalletDetailsViewController
        
        
        detailsViewController.mainContainerView.alpha = 0
        detailsViewController.topContainerView.alpha = 0
        
        containerView.addSubview(detailsViewController.view)
        
        
        let coinView: WalletCoinItemView = WalletCoinItemView.viewWithNib()
        coinView.setup(with: detailsViewController.coin)
        
        let initialFrame = fromCoinView.convert(fromCoinView.bounds,
                                                to: walletViewController.view)
        
        containerView.addSubview(coinView)
        coinView.snp.makeConstraints { (make) in
            make.top.equalTo(initialFrame.origin.y)
            make.left.equalTo(initialFrame.origin.x)
            make.width.equalTo(initialFrame.size.width)
            make.height.equalTo(initialFrame.size.height)
        }
        
        containerView.layoutIfNeeded()
        
        UIView.animate(withDuration: 0.3) {
            detailsViewController.mainContainerView.alpha = 1
        }
        
        coinView.closeButton.isHidden = false
        coinView.closeButton.alpha = 0.0
        
        fromCoinView.alpha = 0.0
        
        let animation = CABasicAnimation(keyPath: "cornerRadius")
        animation.fromValue = 8.0
        animation.toValue = 0.0
        animation.duration = 0.5
        coinView.layer.add(animation, forKey: nil)
        
        let targetFrame = detailsViewController.topContainerView.frame
        
        UIView.animate(withDuration: 0.5,
                       delay: 0,
                       usingSpringWithDamping: 0.8,
                       initialSpringVelocity: 0.6,
                       options: UIViewAnimationOptions.curveEaseInOut,
                       animations: {
                        coinView.closeButton.alpha = 1.0
                        coinView.chevronImageView.alpha = 0.0
                        coinView.snp.remakeConstraints { (make) in
                            make.top.equalTo(0)
                            make.left.equalTo(0)
                            make.width.equalTo(targetFrame.size.width)
                            make.height.equalTo(targetFrame.size.height)
                        }
                        
                        containerView.layoutIfNeeded()
                        
        }) { (completed) in
            detailsViewController.topContainerView.alpha = 1
            coinView.removeFromSuperview()
            transitionContext.completeTransition(true)
        }
    }
    
    func animateDissmissTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        
        let fromController = transitionContext.viewController(forKey: .to)!
        
        let authorizedController: AuthorizedTabBarViewController = (fromController as! UINavigationController).topViewController! as! AuthorizedTabBarViewController
        
        let walletViewController: WalletViewController = authorizedController.currentViewController! as! WalletViewController
        let detailsViewController = transitionContext.viewController(forKey: .from) as! WalletDetailsViewController

        containerView.addSubview(fromController.view)
        
        let coinView: WalletCoinItemView = WalletCoinItemView.viewWithNib()
        coinView.setup(with: detailsViewController.coin)
        coinView.cornerRadius = 8.0
        
        containerView.addSubview(coinView)
        coinView.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.left.equalTo(0)
            make.width.equalTo(detailsViewController.topContainerView.frame.size.width)
            make.height.equalTo(detailsViewController.topContainerView.frame.size.height)
        }
        
        let initialFrame = fromCoinView.convert(fromCoinView.bounds, to: walletViewController.view)
        containerView.layoutIfNeeded()
        
        UIView.animate(withDuration: 0.3) {
            detailsViewController.mainContainerView.alpha = 0
            detailsViewController.topContainerView.alpha = 0
        }
        
        coinView.closeButton.isHidden = false
        coinView.chevronImageView.isHidden = false
        coinView.chevronImageView.alpha = 0.0
        
        fromCoinView.alpha = 0.0
        
        UIView.animate(withDuration: 0.5,
                       delay: 0,
                       usingSpringWithDamping: 0.8,
                       initialSpringVelocity: 0.6,
                       options: UIViewAnimationOptions.curveEaseInOut,
                       animations: {
                        coinView.closeButton.alpha = 0.0
                        coinView.chevronImageView.alpha = 1.0
                        
                        coinView.snp.remakeConstraints { (make) in
                            make.top.equalTo(initialFrame.origin.y)
                            make.left.equalTo(initialFrame.origin.x)
                            make.width.equalTo(initialFrame.size.width)
                            make.height.equalTo(initialFrame.size.height)
                        }
                        containerView.layoutIfNeeded()
                        
        }) { (completed) in
            self.fromCoinView.alpha = 1.0
            coinView.removeFromSuperview()
            transitionContext.completeTransition(true)
        }
    }
}
