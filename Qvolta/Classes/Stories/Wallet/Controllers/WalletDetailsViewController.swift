//
//  WalletDetailsViewController.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/2/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import UIKit
import SnapKit

import Result
import ReactiveCocoa
import ReactiveSwift

class WalletDetailsViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var topContainerView: UIView!
    @IBOutlet weak var mainContainerView: UIView!
    
    @IBOutlet weak var boughtSoldView: UIView!
    
    @IBOutlet weak var circleTurnoverCoinView: CircleWalletDetailsView!
    @IBOutlet weak var circleTurnoverCashView: CircleWalletDetailsView!
    
    @IBOutlet weak var currentRateTitleLabel: UILabel!
    @IBOutlet weak var currentRate: UILabel!
    @IBOutlet weak var currentRatePercentContainerView: UIView!
    @IBOutlet weak var currentRatePercentLabel: UILabel!
    @IBOutlet weak var currentRateIndicatorImageView: UIImageView!
    
    @IBOutlet weak var historyTitleLabel: UILabel!
    @IBOutlet weak var historyButton: UnderlineButton!
    @IBOutlet weak var pendingButton: UnderlineButton!
    
    @IBOutlet weak var historyTableView: UITableView!
    
    @IBOutlet weak var historyShowMoreButton: UIButton!
    
    let sendReceiveAnimator: WalletModalDraggableAnimator = WalletModalDraggableAnimator()
    
    var coin: Coin!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        theme()
        setupCoinView()
        
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    func setupCoinView() {
        let coinView: WalletCoinItemView = WalletCoinItemView.viewWithNib()
        coinView.setup(with: coin)
        coinView.closeButton.isHidden = false
        coinView.chevronImageView.isHidden = true
        
        let action = Action<(), (), NoError> {
            return SignalProducer({ [weak self] observer, _ in
                self?.dismiss(animated: true, completion: nil)
            })
        }
        
        coinView.closeButton.reactive.pressed = CocoaAction(action)
        
        topContainerView.addSubview(coinView)
        coinView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        view.layoutIfNeeded()
        
        coinView.receiveButton.reactive
            .controlEvents(UIControlEvents.touchUpInside).observeValues({
                [weak self] (view) in
                guard let coin = self?.coin else { return }
                
                self?.showReceive(coin: coin)
            })
        
        coinView.sendButton.reactive
            .controlEvents(UIControlEvents.touchUpInside).observeValues({
                [weak self] (view) in
                guard let coin = self?.coin else { return }
                
                self?.showSend(coin: coin)
            })
    }
    
    func theme() {
        mainContainerView.backgroundColor = .gray4
        boughtSoldView.layer.shadowColor = UIColor.lightBlueGrey50.cgColor
        boughtSoldView.layer.shadowRadius = 10
        boughtSoldView.layer.shadowOpacity = 1.0
        
        currentRateTitleLabel.textColor = .gray2
        currentRate.textColor = .gray1
        currentRatePercentContainerView.layer.cornerRadius = 8
        currentRatePercentContainerView.layer.borderColor = UIColor.systemRed.cgColor
        currentRatePercentContainerView.layer.borderWidth = 1
        currentRatePercentLabel.textColor = .systemRed
        
        historyTitleLabel.textColor = .gray2
        historyButton.setTitleColor(.gray2, for: .normal)
        pendingButton.setTitleColor(.gray2, for: .normal)
        historyButton.setTitleColor(.brandActive, for: .selected)
        pendingButton.setTitleColor(.brandActive, for: .selected)
        historyShowMoreButton.setTitleColor(.brandActive, for: .normal)
        
        circleTurnoverCoinView.setup(colors: coin.settings.firstColor,
                                     second: coin.settings.secondColor,
                                     title: "Turnover, \(coin.settings.code)",
            count: "45")
    }

    func showReceive(coin: Coin) {
        let viewController = storyboard?.instantiateViewController(withIdentifier: "WalletReceiveViewController") as! WalletReceiveViewController
        
        viewController.coin = coin
        viewController.modalPresentationStyle = .overFullScreen
        
        viewController.transitioningDelegate = self
        present(viewController, animated: true, completion: nil)
    }
    
    func showSend(coin: Coin) {
        let viewController = storyboard?.instantiateViewController(withIdentifier: "WalletSellViewController") as! WalletSellViewController
        
        viewController.coin = coin
        viewController.modalPresentationStyle = .overFullScreen
        
        viewController.transitioningDelegate = self
        present(viewController, animated: true, completion: nil)
    }
}

extension WalletDetailsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
}

extension WalletDetailsViewController: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        sendReceiveAnimator.isPresenting = true
        
        return sendReceiveAnimator
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
            sendReceiveAnimator.isPresenting = false
        
            return sendReceiveAnimator
    }
}
