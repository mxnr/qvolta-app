//
//  WalletViewModel.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/4/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import Foundation

class WalletViewModel {
    var coins: [Coin]! = [Coin.BTC, Coin.ETH]
}
