//
//  WalletViewController.swift
//  Qvolta
//
//  Created by Andrii Gusarov on 2/2/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import UIKit
import SnapKit

import ReactiveSwift
import ReactiveCocoa

class WalletViewController: UIViewController {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var coinsStackView: UIStackView!
    
    let viewModel: WalletViewModel = WalletViewModel()
    
    let coinAnimator: WalletDetailsAnimator = WalletDetailsAnimator()
    let sendReceiveAnimator: WalletModalDraggableAnimator = WalletModalDraggableAnimator()
    
    var isPresentingCoin: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCoins()
        
        view.backgroundColor = .gray4
        scrollView.backgroundColor = .clear
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UIApplication.shared.statusBarStyle = .default
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func setupCoins() {
        viewModel.coins.forEach { (coin) in
            let coinView: WalletCoinItemView = WalletCoinItemView.viewWithNib()
            coinView.setup(with: coin)
            coinView.cornerRadius = 8.0
            coinsStackView.addArrangedSubview(coinView)
            
            coinView.snp.makeConstraints({ (make) in
                make.height.equalTo(175)
            })
            
            coinView.reactive.controlEvents(UIControlEvents.touchUpInside).observeValues({ [weak self] (view) in
                self?.coinAnimator.fromCoinView = view
                self?.isPresentingCoin = true
                self?.showDetailsFor(coin: view.coin)
                print(view)
            })
            
            coinView.receiveButton.reactive
                .controlEvents(UIControlEvents.touchUpInside).observeValues({
                    [weak self] (view) in
                    self?.isPresentingCoin = false
                    self?.showReceive(coin: coin)
                })

            coinView.sendButton.reactive
                .controlEvents(UIControlEvents.touchUpInside).observeValues({
                    [weak self] (view) in
                        self?.isPresentingCoin = false
                        self?.showSend(coin: coin)
                })
        }
    }
    
    func showReceive(coin: Coin) {
        let viewController = storyboard?.instantiateViewController(withIdentifier: "WalletReceiveViewController") as! WalletReceiveViewController
        
        viewController.coin = coin
        viewController.modalPresentationStyle = .overFullScreen
        
        viewController.transitioningDelegate = self
        present(viewController, animated: true, completion: nil)
    }
    
    func showSend(coin: Coin) {
        let viewController = storyboard?.instantiateViewController(withIdentifier: "WalletSellViewController") as! WalletSellViewController
        
        viewController.coin = coin
        viewController.modalPresentationStyle = .overFullScreen
        
        viewController.transitioningDelegate = self
        present(viewController, animated: true, completion: nil)
    }
    
    func showDetailsFor(coin: Coin) {
        let viewController = storyboard?.instantiateViewController(withIdentifier: "WalletDetailsViewController") as! WalletDetailsViewController
        
        viewController.coin = coin
        
        viewController.transitioningDelegate = self
        present(viewController, animated: true, completion: nil)
    }
}

extension WalletViewController: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        if isPresentingCoin == false {
            sendReceiveAnimator.isPresenting = true
            return sendReceiveAnimator
        }
        
        coinAnimator.isPresenting = true
        
        return coinAnimator
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        if isPresentingCoin == false {
            sendReceiveAnimator.isPresenting = false
            return sendReceiveAnimator
        }
        
        coinAnimator.isPresenting = false
        
        return coinAnimator
    }
}
