//
//  ServiceLocatorTests.swift
//  QvoltaTests
//
//  Created by Andrii Gusarov on 1/10/18.
//  Copyright © 2018 Qvolta. All rights reserved.
//

import XCTest
import Nimble

@testable import QvoltaDev

class TestClass {}
protocol TestProtocol {}
class TestClassProtocol: TestProtocol {}

class ServiceLocatorTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testServiceLocatorShouldReturnServiceForExactGivenType() {
        // Given
        let testClass = TestClass()
        
        // When
        ServiceLocator.shared.set(testClass)
        
        // Then
        let returnClass: TestClass? = ServiceLocator.shared.get()
        expect(returnClass).to(beAnInstanceOf(TestClass.self))
    }
    
    func testServiceLocatorShouldReturnServiceForExactGivenTypeOfProtocol() {
        // Given
        let testClass = TestClassProtocol()
        
        // When
        ServiceLocator.shared.set(testClass as TestProtocol)
        
        // Then
        let returnClass: TestProtocol? = ServiceLocator.shared.get()
        expect(returnClass).to(beAnInstanceOf(TestClassProtocol.self))
    }
    
    func testServiceLocatorShouldntReturnServiceForDifferentFromInitiallySettedProtocol() {
        // Given
        let testClass = TestClassProtocol()
        
        // When
        ServiceLocator.shared.set(testClass as TestProtocol)
        
        // Then
        let returnClass: TestClassProtocol? = ServiceLocator.shared.get()
        expect(returnClass).to(beNil())
    }
    
}
